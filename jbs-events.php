<?php
/*
  Plugin Name: JBS Events
  Plugin URI:  https://jakob-bennemann.de/
  Description: Dieses Plugin stellt eine einfach Art der Teilnahmeveraltung dar und bietet somit einen idealen Weg zur Planung von Veranstaltungen. Es können Veranstaltungen erstellt und es kann festgelegt werden, ob Benutzer der Seite an diesen Veranstaltungen teilnehmen können.
  Weiterhin können Veranstaltungen auch über einen Zeitraum von mehreren Tagen angelegt werden.
  Version:     1.3.7
  Author:      Jakob Bennemann
  Author URI:  https://jakob-bennemann.de
  License:     GPL2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
  Domain Path: /languages
 */

// direkten Zugriff verweigern
defined('ABSPATH') or die('No script kiddies please!');

add_action('in_admin_header', 'jbs_admin_header');
function jbs_admin_header(){
    echo "";
}

global $jbs_db_version;
$jbs_db_version = '1.5';

global $wpms_options; 
$wpms_options = array (
    'smtp-mail' => '',
    'smtp-name' => '',
    'mailer' => 'smtp',
    'mail_set_return_path' => 'false',
    'smtp_host' => 'localhost',
    'smtp-port' => '587',
    'smtp_ssl' => 'ssl',
    'smtp_auth' => true,
    'smtp_username' => '',
    'smtp_pass' => ''
);


/**
 * Activation function. This function creates the required options and defaults.
 */
if (!function_exists('wp_mail_smtp_activate')) :
function wp_mail_smtp_activate() {
    
    global $wpms_options;
    
    // Create the required options...
    foreach ($wpms_options as $name => $val) {
        add_option($name,$val);
    }
    
}
endif;

if (!function_exists('wp_mail_smtp_whitelist_options')) :
function wp_mail_smtp_whitelist_options($whitelist_options) {
    
    global $wpms_options;
    
    // Add our options to the array
    $whitelist_options['email'] = array_keys($wpms_options);
    
    return $whitelist_options;
    
}
endif;

// if(!function_exists('set_content_type')):
// function set_content_type($content_type){
//     return 'text/html';
// }
// endif;
// add_filter('wp_mail_content_type','set_content_type');

// To avoid any (very unlikely) clashes, check if the function alredy exists
if (!function_exists('phpmailer_init_smtp')) :
function phpmailer_init_smtp($phpmailer) {
    
    // If constants are defined, apply those options
    if (defined('WPMS_ON') && WPMS_ON) {
        
        $phpmailer->Mailer = WPMS_MAILER;
        
        if (WPMS_SET_RETURN_PATH)
            $phpmailer->Sender = $phpmailer->From;
        
        if (WPMS_MAILER == 'smtp') {
            $phpmailer->SMTPSecure = WPMS_SSL;
            $phpmailer->Host = WPMS_SMTP_HOST;
            $phpmailer->Port = WPMS_SMTP_PORT;
            if (WPMS_SMTP_AUTH) {
                $phpmailer->SMTPAuth = true;
                $phpmailer->Username = WPMS_SMTP_USER;
                $phpmailer->Password = WPMS_SMTP_PASS;
            }
        }
        
        // If you're using contstants, set any custom options here
        $phpmailer = apply_filters('wp_mail_smtp_custom_options', $phpmailer);
        
    }
    else {
        
        // Check that mailer is not blank, and if mailer=smtp, host is not blank
        if ( ! get_option('mailer') || ( get_option('mailer') == 'smtp' && ! get_option('smtp_host') ) ) {
            return;
        }
        
        // Set the mailer type as per config above, this overrides the already called isMail method
        $phpmailer->Mailer = get_option('mailer');

        if(get_option('mail_from') && get_option('mail_from_name')){
            $phpmailer->setFrom( get_option('mail_from') , get_option('mail_from_name') );
            $phpmailer->addReplyTo( get_option('mail_from') , get_option('mail_from_name') );
        }
        
        // Set the Sender (return-path) if required
        if (get_option('mail_set_return_path'))
            $phpmailer->Sender = $phpmailer->From;
        
        // Set the SMTPSecure value, if set to none, leave this blank
        $phpmailer->SMTPSecure = get_option('smtp_ssl') == 'none' ? '' : get_option('smtp_ssl');
        
        // If we're sending via SMTP, set the host
        if (get_option('mailer') == "smtp") {
            
            // Set the SMTPSecure value, if set to none, leave this blank
            $phpmailer->SMTPSecure = get_option('smtp_ssl') == 'none' ? '' : get_option('smtp_ssl');
            
            // Set the other options
            $phpmailer->Host = get_option('smtp_host');
            $phpmailer->Port = get_option('smtp_port');
            
            // If we're using smtp auth, set the username & password
            if (get_option('smtp_auth') == "true") {
                $phpmailer->SMTPAuth = TRUE;
                $phpmailer->Username = get_option('smtp_user');
                $phpmailer->Password = get_option('smtp_pass');
            }
        }
        
        // You can add your own options here, see the phpmailer documentation for more info:
        // http://phpmailer.sourceforge.net/docs/
        $phpmailer = apply_filters('wp_mail_smtp_custom_options', $phpmailer);

    }
    
} // End of phpmailer_init_smtp() function definition
endif;


/**
 * This function sets the from email value
 */
if (!function_exists('wp_mail_smtp_mail_from')) :
function wp_mail_smtp_mail_from ($orig) {
    
    // Get the site domain and get rid of www.
    $sitename = strtolower( $_SERVER['SERVER_NAME'] );
    if ( substr( $sitename, 0, 4 ) == 'www.' ) {
        $sitename = substr( $sitename, 4 );
    }

    $default_from = 'wordpress@' . $sitename;
    // End of copied code
    
    // If the from email is not the default, return it unchanged
    if ( $orig != $default_from ) {
        return $orig;
    }
    
    if (defined('WPMS_ON') && WPMS_ON) {
        if (defined('WPMS_MAIL_FROM') && WPMS_MAIL_FROM != false)
            return WPMS_MAIL_FROM;
    }
    elseif (is_email(get_option('mail_from'), false))
        return get_option('mail_from');
    
    // If in doubt, return the original value
    return $orig;
    
} // End of wp_mail_smtp_mail_from() function definition
endif;


/**
 * This function sets the from name value
 */
if (!function_exists('wp_mail_smtp_mail_from_name')) :
function wp_mail_smtp_mail_from_name ($orig) {
    
    // Only filter if the from name is the default
    if ($orig == 'WordPress') {
        if (defined('WPMS_ON') && WPMS_ON) {
            if (defined('WPMS_MAIL_FROM_NAME') && WPMS_MAIL_FROM_NAME != false)
                return WPMS_MAIL_FROM_NAME;
        }
        elseif ( get_option('mail_from_name') != "" && is_string(get_option('mail_from_name')) )
            return get_option('mail_from_name');
    }
    
    // If in doubt, return the original value
    return $orig;
    
} // End of wp_mail_smtp_mail_from_name() function definition
endif;

function wp_mail_plugin_action_links( $links, $file ) {
    if ( $file != plugin_basename( __FILE__ ))
        return $links;

    $settings_link = '<a href="options-general.php?page=' . plugin_basename(__FILE__) . '">' . __( 'Settings', 'wp_mail_smtp' ) . '</a>';

    array_unshift( $links, $settings_link );

    return $links;
}

// Add an action on phpmailer_init
add_action('phpmailer_init','phpmailer_init_smtp');

if (!defined('WPMS_ON') || !WPMS_ON) {
    // Whitelist our options
    add_filter('whitelist_options', 'wp_mail_smtp_whitelist_options');
    // Add the create pages options
    // add_action('admin_menu','wp_mail_smtp_menus');
    // // Add an activation hook for this plugin
    // register_activation_hook(__FILE__,'wp_mail_smtp_activate');
    // Adds "Settings" link to the plugin action page
    add_filter( 'plugin_action_links', 'wp_mail_plugin_action_links',10,2);
}

// Add filters to replace the mail from name and emailaddress
add_filter('wp_mail_from','wp_mail_smtp_mail_from');
add_filter('wp_mail_from_name','wp_mail_smtp_mail_from_name');

//laden anderer, benötigter Dateien
include_once(dirname(__FILE__) . '/includes/class-jbs-list.php');

if ( is_admin() ) {
     // We are in admin mode
     require_once( dirname(__file__).'/admin/jbs-admin-init.php' );
     require_once( dirname(__file__).'/admin/jbs-log.php' );
     if(get_option('intern-guest') == 'on')
     require_once( dirname(__file__).'/admin/jbs-admin-invisible.php' );
     require_once( dirname(__file__).'/admin/jbs-register-settings.php' );
}


//Activation Hook
if( !function_exists('jbs_activation') ){
    function jbs_activation() {
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        global $wpdb;
        global $jbs_db_version;
        global $wpms_options;
        $installed_ver = get_option('jbs_db_version');

        if ($installed_ver != $jbs_db_version) {
            $charset_collate = $wpdb->get_charset_collate();
            $table_name = $wpdb->prefix . "jbs_events";
            $table_name3 = $wpdb->prefix . "jbs_events_log";
            $table_name2 = $wpdb->prefix . "jbs_events_participation";

            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                date date NOT NULL,
                description varchar(255) NOT NULL,
                time_start varchar(5),
                time_end varchar(5),
                participation bit NOT NULL,
                extra_info text,
                info_participant bit NOT NULL,
                link text,
                belongs_to_event mediumint(9),
                notification mediumint(3),
                group_aktiv mediumint(1),
                group_passiv mediumint(1),
                group_ehren mediumint(1),
                group_sonstige mediumint(1),
                UNIQUE KEY id (id)) $charset_collate;";
            dbDelta($sql);

            $sql = "CREATE TABLE $table_name2 (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                userid mediumint(9) NOT NULL,
                eventid mediumint(9) NOT NULL,
                status mediumint(1) NOT NULL,
                info_participant varchar(255),
                UNIQUE KEY id (id)) $charset_collate;";
            dbDelta($sql);

            $sql = "CREATE TABLE $table_name3 (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                time datetime NOT NULL,
                action text NOT NULL,
                info text NOT NULL,
                userid mediumint(9),
                user text,
                ip text NOT NULL,
                UNIQUE KEY id (id)) $charset_collate;";
            dbDelta($sql);

            update_option('$jbs_db_version', $jbs_db_version);

                
            // Create the required options...
            foreach ($wpms_options as $name => $val) {
               add_option($name,$val);
            }
        }
    }


}
register_activation_hook(__FILE__, 'jbs_activation');

//Deactivation Hook
if( !function_exists('jbs_deactivation')){
    function jbs_deactivation(){
        flush_rewrite_rules();
    }

}
register_deactivation_hook(__FILE__, 'jbs_deactivation');



function custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="'.plugin_dir_url(__FILE__).'custom-login/custom-login.css" />'; 
}
add_action('login_head', 'custom_login');



include_once(dirname(__FILE__) . '/includes/class-jbs-shortcodes.php');
include_once(dirname(__FILE__) . '/includes/jbs-registration.php');
include_once(dirname(__FILE__) . '/includes/class-jbs-events-widget.php');
if(!is_admin()){
    if(get_option('intern-guest') == 'on')
    include_once(dirname(__FILE__) . '/includes/jbs-invisible.php');
}

load_plugin_textdomain('jbs-events', false, basename(dirname(__FILE__)) . '/languages');



//        add_action('jbs_send_notification_mail', 'send_notification_mail');

//function to redirect after logout
function jbs_logout() {
	wp_redirect(get_permalink(get_option('page-guests-redirect')));
    exit;
}

//hook function  to wp_logout action
add_action('wp_logout', 'jbs_logout');


function jbs_update_db_check() {
    global $jbs_db_version;
    if (get_site_option('jbs_db_version') != $jbs_db_version) {
        jbs_activation();
    }
}

add_action('plugins_loaded', 'jbs_update_db_check');

function jbs_initial_data() {

    global $wpdb;
    $table_name = $wpdb->prefix . "jbs_events";
    //adding initial data
    $descr = 'JBS Event 1';
    $welcome_text = 'Congratulations, you just completed the installation!';
    $timestamp = time();

    $wpdb->insert(
            $table_name, array(
        'date' => date('Y-m-d', $timestamp),
        'description' => $descr,
        'time_start' => date("H:i", $timestamp),
        'time_end' => date("H:i", strtotime("+4 hours")),
        'participation' => 1,
        'extra_info' => $welcome_text,
        'info_participant' => 0)
    );
}

function enqueue_styles() {
    wp_enqueue_style('jbs-custom-style', plugins_url() . '/jbs-events/css/jbs-custom.css');
    wp_enqueue_style('jbs-custom-style-fa', plugins_url() . '/jbs-events/css/font-awesome.min.css');
}

add_action('wp_enqueue_scripts', 'enqueue_styles');
add_action('admin_enqueue_scripts', 'enqueue_styles');

add_action('wp_head', 'jbs_head');
function jbs_head(){
	$style='<style type="text/css">';
	$style.= ''.get_option('jbs-events-css').'';
	$style .= '</style>';

    //$style.="<link href='//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css' rel='stylesheet'>";
    //$style.="<link href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css' rel='stylesheet'>";

	echo $style;?>
    <script>
    var headertext = [];
    var headers = document.querySelectorAll("thead");
    var tablebody = document.querySelectorAll("tbody");

    for (var i = 0; i < headers.length; i++) {
    headertext[i]=[];
    for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
    var current = headrow;
    headertext[i].push(current.textContent);
    }
    }

    for (var h = 0, tbody; tbody = tablebody[h]; h++) {
    for (var i = 0, row; row = tbody.rows[i]; i++) {
    for (var j = 0, col; col = row.cells[j]; j++) {
    col.setAttribute("data-th", headertext[h][j]);
    }
    }
    }</script>
<?php
}

function jbs_remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('init', 'jbs_remove_admin_bar');

add_action('init', 'start_buffer_output');
function start_buffer_output() {
        ob_start();
}


function my_format_TinyMCE( $in ) {
    $in['remove_linebreaks'] = false;
    $in['force_br_newlines'] = true;
    $in['force_p_newlines'] = false;
    $in['convert_newlines_to_brs'] = true;
    $in['entities'] = '160,nbsp,38,amp,60,lt,62,gt,br';   
    $in['entity_encoding'] = 'named';
    $in['keep_styles'] = true;
    $in['paste_remove_styles'] = false;
    $in['paste_remove_spans'] = false;
    $in['wpautop'] = false;
    $in['apply_source_formatting'] = true;
    $in['block_formats'] = "Paragraph=p; Heading 1=h1; Heading 2=h2; Heading 3=h3; Heading 4=h4; Heading 5=h5; Heading 6=h6;";
    $in['toolbar1'] = 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,wp_fullscreen,wp_adv ';
    
    return $in;
}
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );

function jbs_events_replace_placeholder($content){
    $variables = 
    array(
        'ausrichter' => get_option('knobelturnier-ausrichter'),
        'datum' => get_option('knobelturnier-datum'),
    );
    foreach($variables as $key => $value){
            if($key == 'datum'){
                $format = substr($value, 8,2) . '.';
                $format = $format . substr($value, 5,2) . '.';
                $format = $format . substr($value, 0,4);
                $content = str_replace('{{'.strtoupper($key).'}}', $format, $content);
            }else{             
                $content = str_replace('{{'.strtoupper($key).'}}', $value, $content);
            }
    }
    return $content; 

}
add_filter('the_content', 'jbs_events_replace_placeholder', 15);


/*
* auf wp-admin.php Seite prüfen, ob Benutzer aktivert wurde. Wenn nicht, kein Login!
*/
add_filter('authenticate', 'check_login', 100, 3);
function check_login($user, $username, $password) {
    // this filter is called on the log in page
    // make sure we have a username before we move forward
    if (!empty($username)) {
        $user_data = $user->data;

        if ( get_user_meta( $user->ID, 'approval_status', true ) != 1) {
          // stop login
          return null;
        }
        else {
            return $user;
        }
    }

    return $user;
}

    //2. Add validation. In this case, we make sure first_name is required.
    add_filter( 'registration_errors', 'jbs_registration_errors', 10, 3 );
    function jbs_registration_errors( $errors, $sanitized_user_login, $user_email ) {
        

        //blacklist_array erstellen
        $blacklist = "";
        $list = get_option('register-blacklist');
        if( !empty($list) ){
            $domains = explode(',', $list);
        }
        foreach ($domains as $domain) {
            $blacklist[] = strtolower($domain);
        }
        if ( is_email($user_email) ) {
            $parts = explode('@', $user_email);
            $parts = explode('.', $parts[count($parts)-1]);
            $domain = $parts[count($parts)-1];
            if ( in_array(strtolower($domain), $blacklist) ) {
                $errors->add('email_domain_blacklisted', __('Die E-Mail Domain ist nicht zugelassen', 'jbs-events'));

                insertLogDB('New Spam WP User decliened', null, 'email:' . $user_email, null, null, $_SERVER['REMOTE_ADDR']);
            }
            insertLogDB('New Registration during WP Form', null, 'email:' . $user_email, null, null, $_SERVER['REMOTE_ADDR']);
        }

        return $errors;
    }

// Ende
?>
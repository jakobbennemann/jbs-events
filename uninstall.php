<?php 
// If uninstall is not called from WordPress, exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}
 
 // List of all jbs_options including user_meta options preferedly as an array
$option_name = [
  'jbs-knobelturnier',
  ];

//loop through the array to delete all options
delete_option( $option_name );
 
// For site options in Multisite
delete_site_option( $option_name );  
 
// Drop all custom db tables
global $wpdb;
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}jbs_events" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}jbs_events_log" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}jbs_events_participation" );global $wpdb;
$removesecret = $wpdb->query("DELETE FROM $wpdb->postmeta WHERE meta_key = '_secret_new_field'");
if ($removesecret)
{
  // awsome
}
else
{
  // whatever
}

?>
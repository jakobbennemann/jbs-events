<?php
// Creating the widget 
class jbs_events_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'jbs_events_widget', 

    // Widget name will appear in UI
    __('Termine', 'jbs-events'), 

    // Widget description
    array( 'description' => __( 'Zeigt die nächsten 3 Termine an', 'jbs-events' ), ) 
    );
  }

  // Creating widget front-end
  // This is where the action happens
  public function widget( $args, $instance ) {
    global $wpdb;
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];

    // This is where you run the code and display the output
    //echo __( 'Hello, World!', 'jbs-events' );

    //get next 5 Events From db
    $today = date('Y-m-d');
    $table = $wpdb->prefix . "jbs_events";
    $result = $wpdb->get_results( "SELECT * FROM $table WHERE date >= CURDATE() ORDER BY date ASC LIMIT 3;" );
    echo "<table class='jbs_widget_table'>";
    foreach ($result as $row) {
      $year = date('Y', strtotime($row->date));
      $month = date('m', strtotime($row->date));
      $day = date('d', strtotime($row->date));
 
      $test_datum = $row->date;
      $wochentage = array('Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag');
      list ($jahr, $monat, $tag) = explode('-', $test_datum);
      $datum = getdate(mktime(0, 0, 0, $monat, $tag , $jahr));
      $wochentag = $datum['wday'];
      echo "<tr>
              <td class='date'>
                <span class='' style='color:#e1e1e1;font-weight:bold;'>$wochentage[$wochentag], $day.$month.$year
                </span>
              </td>
              <td class=''>";
                if($row->link != NULL && $row->link != ''){
                  echo "<span><a style='text-decoration:underline;color:#f2f2f2;' href=\"$row->link\">".$row->description."</a></span>";
                  //echo "<br />teilnehmen?";
                }else{
                  echo "<span style='color:#f2f2f2;'>".$row->description."</span>";
                }
              echo '</td>
            </tr>';
    }
    echo '</table>';
    if($today){}
    echo $args['after_widget'];
  }
      
  // Widget Backend 
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    }
    else {
      $title = __( 'Nächsten Termine', 'jbs-events' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php 
  }
    
  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
  register_widget( 'jbs_events_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
?>
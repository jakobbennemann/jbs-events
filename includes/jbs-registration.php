<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php' );

require_once (ABSPATH . WPINC . '/pluggable.php');

function send_deactivation_mail($current_user){
    $time = date("Y-m-d H:i:s");

    //send de-activation mail to user
    $mail = $current_user->user_email;
    $message = "Hallo " . $current_user->display_name . ',<br />';
    $message .= "dein Account wurde deaktiviert.<br />Du kannst dich nicht weiter in unseren internen Bereich einloggen.<br />";
    $message .= "<br /><br />Gut Spiel<br />Spielmannszug Südlohn 1950 e.V.";

    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail($mail,'Dein Account wurde deaktiviert',$message, $headers);
    insertLogDB('Disapproval mail sent', $time, '|user-id:' . $current_user->ID, null, null, $_SERVER['REMOTE_ADDR']);

    return;
}

function send_activation_mail($current_user){
    $time = date("Y-m-d H:i:s");
    //send activation mail to user
    $mail = $current_user->user_email;
    $message = "Hallo " . $current_user->display_name . ',<br />';
    $message .= "du wurdest freigeschaltet und kannst dich ab sofort einloggen.<br />";
    $message .= "<br /><br />Gut Spiel<br />Spielmannszug Südlohn 1950 e.V.";

    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail($mail,'Du wurdest freigeschaltet',$message, $headers);
    insertLogDB('Approval mail sent', $time, '|user-id:' . $current_user->ID, null, null, $_SERVER['REMOTE_ADDR']);
    return;
}

function send_delete_account_mail($current_user){
    $time = date("Y-m-d H:i:s");
    //send activation mail to user
    $mail = $current_user->user_email;
    $message = "Hallo " . $current_user->display_name . ',<br />';
    $message .= "dein Account wurde gelöscht.<br />";
    $message .= "<br /><br />Gut Spiel<br />Spielmannszug Südlohn 1950 e.V.";

    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail($mail,'Dein Account wurde gelöscht',$message, $headers);
    insertLogDB('Account deleted mail sent', $time, '|user-id:' . $current_user->ID, null, null, $_SERVER['REMOTE_ADDR']);
    return;
}

/*
##########  Benutzer aktivieren im Backend
*/
if (isset($_GET['action']) && $_GET['action'] == 'activate' && isset($_GET['user_id'])) {
    update_user_meta($_GET['user_id'], 'approval_status', 1);

    global $current_user;
    wp_get_current_user();
    $nutzer = get_userdata($_GET['user_id']);
    $time = date("Y-m-d H:i:s");
    insertLogDB('Approvalstatus changed', $time, 'new Status:' . 1 . '|user-id:' . $_GET['user_id'], $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);

    send_activation_mail($nutzer);

    wp_redirect(admin_url('users.php', 'https'));
    exit();
} else

##########  Benutzer deaktivieren im Backend

if (isset($_GET['action']) && $_GET['action'] == 'deactivate' && isset($_GET['user_id'])) {
    update_user_meta($_GET['user_id'], 'approval_status', 0);

    global $current_user;
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    $nutzer = get_userdata($_GET['user_id']);
    insertLogDB('Approvalstatus changed', $time, 'new Status:' . 0 . '|user-id:' . $_GET['user_id'], $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);

    send_deactivation_mail($nutzer);

    wp_redirect(admin_url('users.php', 'https'));
    exit();
}

##########  Benutzer löschen im Backend
function jbs_delete_user( $user_id ){
    global $current_user;
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    $nutzer = get_userdata($user_id);
    insertLogDB('User deleted', $time, 'new Status: deleted |user-id:' . $user_id, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);

    send_delete_account_mail($nutzer);

}
add_action( 'delete_user', 'jbs_delete_user' );


/*
##########  Logeintrag in DB schreiben
*/
function insertLogDB($action, $time, $info, $id, $name, $ip) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_log';
    $wpdb->insert($table, array(
        'time' => $time,
        'action' => $action,
        'info' => $info,
        'userid' => $id,
        'user' => $name,
        'ip' => $ip
            ), array(
        '%s', '%s', '%s', '%d', '%s', '%s'
    ));
}

// user registration login form
function jbs_registration_form() {

// only show the registration form to non-logged-in members
    if (!is_user_logged_in()) {

        global $jbs_load_css;

// set this to true so the CSS is loaded
        $jbs_load_css = true;

// check to make sure user registration is enabled
        $registration_enabled = get_option('users_can_register');

// only show the registration form if allowed
        if ($registration_enabled) {
            $output = jbs_registration_form_fields();
        } else {
            $output = __('<span class="reg_closed" style="font-weight:bold;color:#900;">Registrierungen sind zur Zeit geschlossen!<br />Bei Fragen wende dich an den Administrator.</span>');
        }
        return $output;
    }
}

add_shortcode('jbs_register', 'jbs_registration_form');

// user login form
function jbs_login_form() {

    if (!is_user_logged_in()) {

        global $jbs_load_css;

// set this to true so the CSS is loaded
        $jbs_load_css = true;

        $output = jbs_login_form_fields();
    } else {
// could show some logged in user info here
// $output = 'user info here';
    }
    return $output;
}

add_shortcode('jbs_login', 'jbs_login_form');

// registration form fields
function jbs_registration_form_fields() {

    ob_start();
    ?>	
    <h3 class="jbs_header"><?php _e('Für den internen Bereich registrieren'); ?></h3>

    <?php
// show any error messages after form submission
    jbs_show_error_messages();
    jbs_show_success_messages();
    ?>

    <form id="jbs_registration_form" class="jbs_form" action="" method="POST">
        <fieldset>
            <?php if(get_option('register-username') == 'on'){ ?>
            <p>
                <label for="jbs_user_Login"><?php _e('Benutzername'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_login" id="jbs_user_login" class="required" required type="text"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-email') == 'on'){ ?>
            <p>
                <label for="jbs_user_email"><?php _e('E-Mail'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_email" id="jbs_user_email" class="required" required type="email"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-first-name') == 'on'){ ?>
            <p>
                <label for="jbs_user_first"><?php _e('Vorname'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_first" id="jbs_user_first" required type="text"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-last-name') == 'on'){ ?>
            <p>
                <label for="jbs_user_last"><?php _e('Nachname'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_last" id="jbs_user_last" required type="text"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-nr') == 'on' || get_option('register-street') == 'on'){ ?>
            <p>
            <?php if(get_option('register-street') == 'on'){ ?>
                <label for="jbs_user_street"><?php _e('Straße'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_street" id="jbs_user_street" required type="text"/>
            <?php ;} ?>
            <?php if(get_option('register-nr') == 'on'){ ?>
                <label for="jbs_user_street_nr"><?php _e('Hausnr.'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_street_nr" id="jbs_user_street_nr" required size="3" type="text"/>
            <?php ;} ?>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-plz') == 'on' || get_option('register-ort') == 'on' ){ ?>
            <p>
            <?php if(get_option('register-plz') == 'on'){ ?>
                <label for="jbs_user_plz"><?php _e('PLZ'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_plz" id="jbs_user_plz" size="8" required type="text"/>
            <?php ;} ?>
            <?php if(get_option('register-ort') == 'on'){ ?>
                <label for="jbs_user_ort"><?php _e('Ort'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_ort" id="jbs_user_ort" type="text" required />
            <?php ;} ?>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-mobil') == 'on'){ ?>
            <p>
                <label for="jbs_user_mobil"><?php _e('Handynummer'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_mobil" id="jbs_user_mobil" required type="text"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-password') == 'on'){ ?>
            <p>
                <label for="password"><?php _e('Passwort'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_pass" id="password" class="required" required type="password"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-password2') == 'on'){ ?>
            <p>
                <label for="password_again"><?php _e('Passwort wiederholen'); ?><span class="form_required">*</span></label>
                <input name="jbs_user_pass_confirm" id="password_again" required class="required" type="password"/>
            </p>
            <?php ;} ?>
            <?php if(get_option('register-instrument') == 'on'){ ?>
            <p>
                <label for="instrument"><?php _e('Instrument'); ?><span class="form_required">*</span></label>
                <select name="instrument" value="" required>
                    <option value="" selected>-- bitte wählen --</option>
                    <option value="Flöte" >Flöte</option>
                    <option value="Schlagwerk">Schlagwerk</option>
                </select>
            </p>
            <?php ;} ?>
            <p>
                <?php wp_nonce_field( 'jbs_register_action', 'jbs_register_nonce' ); ?>
                <input type="submit" value="<?php _e('Jetzt Registrieren'); ?>"/>
            </p>
            <p><span class="required_info" style="font-style:italic;">Mit * markierte Felder müssen ausgefüllt werden.</span></p>
        </fieldset>
    </form>
    <?php
    return ob_get_clean();
}

// login form fields
function jbs_login_form_fields() {

    ob_start();
    
// show any error messages after form submission
    jbs_show_error_messages();
    jbs_show_success_messages();
    ?>

    <form id="jbs_login_form"  class="jbs_form" action="" method="post">
        <?php if (isset($_GET['action']) && $_GET['action'] == "lost_pw") { ?>
            <fieldset>
                <h3 class="jbs_header"><?php _e('Passwort zurücksetzen'); ?></h3>
                <p>Bitte geben Sie Ihre E-Mail Adresse ein:</p>
                <p>
                    <label for="jbs_user_Login"><?php _e('E-Mail Adresse'); ?></label>
                    <input name="jbs_user_email" id="jbs_user_email" class="required" required type="text" />
                </p>
                <p>
                    <input type="hidden" name="jbs_pw_nonce" value="<?php echo wp_create_nonce('jbs-pw-nonce'); ?>"/>
                    <input id="jbs_pw_submit" type="submit" value="Passwort zurücksetzen"/>
                </p>
            </fieldset>

        <?php } else { ?>
            <fieldset>
                <h3 class="jbs_header"><?php _e('Einloggen'); ?></h3>
                <p>
                    <label for="jbs_user_Login"><?php _e('Benutzername'); ?></label>
                    <input name="jbs_user_login" id="jbs_user_login" class="required" required type="text"/>
                </p>
                <p>
                    <label for="jbs_user_pass"><?php _e('Passwort'); ?></label>
                    <input name="jbs_user_pass" id="jbs_user_pass" class="required" required type="password"/>
                </p>
                <p>
                    <input id="jbs_login_submit" name="jbs_login_submit" type="submit" value="Login"/> <a href="?action=lost_pw" >Passwort vergessen?</a>
                </p>
                <p style=""><strong>Hinweis</strong>: Wenn du noch keinen Account hast, kannst du dich <a href="https://www.spielmannszug-suedlohn.de/login/#toggle-id-2">hier registrieren</a> </p>
            </fieldset>
        <?php } ?>
    </form>
    <?php
    return ob_get_clean();
}

function jbs_reset_action() {
    if (isset($_POST['jbs_user_pw']) && wp_verify_nonce($_POST['jbs_change_nonce'], 'jbs-change-nonce')) {
        $key = $_GET['key'];
        $mail = $_GET['email'];
        $pw = $_POST['jbs_user_pw'];
        $pw2 = $_POST['jbs_user_pw2'];
        $salt = "498#2D85B631%3800EBD!801600D*";

        $resetkey = hash('sha512', $salt . $mail);
        if ($resetkey == $key) {
            if ($pw == $pw2) {
                echo "läuft...";
            }
        }
    }
}


add_action('init', 'jbs_reset_action');
add_action('init', 'jbs_reset_form');
function jbs_reset_form() {
    if (isset($_GET['key']) && isset($_GET['email'])) {
        $s = '<div class="wrap">
            <form action ="" method="POST">
                <fieldset>
                    <p>
                        <label for="jbs_user_pw">_e("Passwort"); </label>
                        <input name="jbs_user_pw" id="jbs_user_pw" class="required" required type="password"/>
                    </p>
                    <p>
                        <label for="jbs_user_pw2"><_e("Passwort bestätigen"); </label>
                        <input name="jbs_user_pw2" id="jbs_user_pw2" class="required" required type="password"/>
                    </p>
                    <p>
                        <input type="hidden" name="jbs_change_nonce" value="' . wp_create_nonce("jbs-change-nonce") . '"/>
                               <input type="submit" value=" _e("Paaswort ändern"); "/>
                    </p>
                </fieldset>
            </form>
        </div>';

        return $s;
    }
}


// logs a member in after submitting a form
function jbs_login_member() {

    if (isset( $_POST['jbs_login_submit'] )) {

// this returns the user ID and other info from the user name
        $user = get_userdatabylogin($_POST['jbs_user_login']);

        if (!$user) {
// if the user name doesn't exist
            jbs_errors()->add('empty_username', __('Ungültiger Benutzername'));
        }

        if (!isset($_POST['jbs_user_pass']) || $_POST['jbs_user_pass'] == '') {
// if no password was entered
            jbs_errors()->add('empty_password', __('Bitte gibt dein Passwort ein'));
        }

// check the user's login with their password
        if (!wp_check_password($_POST['jbs_user_pass'], $user->user_pass, $user->ID)) {
// if the password is incorrect for the specified user
            jbs_errors()->add('empty_password', __('Falsches Passwort'));
        }

// retrieve all error messages
        $errors = jbs_errors()->get_error_messages();

// only log the user in if there are no errors
        if (empty($errors)) {
            if (get_user_meta($user->ID, 'approval_status', true) == 1 && get_option('standard-aktivierung') == 'on') {
//User login - approved user
                //wp_set_auth_cookie( $user->ID, true );
                //wp_setcookie($_POST['jbs_user_login'], $_POST['jbs_user_pass'], true);
                //wp_set_current_user($user->ID, $_POST['jbs_user_login']);
                //do_action('wp_login', $_POST['jbs_user_login']);
                /*wp_signon([
                        'user_login' => $_POST['jbs_user_login'],
                        'user_password' => $_POST['jbs_user_pass']
                ]);*/


                wp_set_current_user( $user->ID, $user->user_login );
                wp_set_auth_cookie( $user->ID, true );
                do_action( 'wp_login', $user->user_login );


                $time = date("Y-m-d H:i:s");
                insertLogDB('Login Successful', $time, 'user:' . $_POST['jbs_user_login'], $user->ID, $user->display_name, $_SERVER['REMOTE_ADDR']);


                wp_redirect(get_permalink(get_option('page-users-redirect')));
                exit;
            } else {


                if(get_user_meta($user->ID, 'approval_status', true) == 0 && get_option('standard-aktivierung') == 'on'){
                    jbs_errors()->add('deactivated_user', __('Dein Account ist noch nicht freigeschaltet worden.'));
                }

                $time = date("Y-m-d H:i:s");
                insertLogDB('Login Error', $time, 'user:' . $_POST['jbs_user_login'] . '|reason:not activated', $user->ID, $user->display_name, $_SERVER['REMOTE_ADDR']);
            }
        } else {
            $time = date("Y-m-d H:i:s");
            insertLogDB('Login Error', $time, 'login:' . $_POST['jbs_user_login'] . '|reason:wrong credentials', NULL, NULL, $_SERVER['REMOTE_ADDR']);
        }
    } else
    if (isset($_POST['jbs_user_email']) && wp_verify_nonce($_POST['jbs_pw_nonce'], 'jbs-pw-nonce')) {
// Passwort zurücksetzen
        global $wpdb;

// Pürfe, ob username gültig
        $table = $wpdb->prefix . 'users';
        $result = $wpdb->get_row("SELECT * FROM $table WHERE user_email = '" . $_POST['jbs_user_email'] . "'", ARRAY_A);

        if ($result != null) {
//Email vorhanden
    //        $salt = "498#2D85B631%3800EBD!801600D*";
			//echo $result['user_email'];

//unique userpass reset key
    //        $passwort = hash('sha512', $salt . $result['user_email']);
    //        $pwrurl = ''.site_url() . '/wp-content/plugins/jbsorganizer/includes/jbs-registration.php?email=' . $_POST['jbs_user_email'] . '?key=' . $passwort;
            //echo "$pwrurl";
//email senden mit pwr link
	$mail = $result['user_email'];
	//$betreff = 'testbetreff';
	
	/* einfache Variante zum PW Reset */
	$pool = "qwertzuiopasdfghjklyxcvbnm";
	$pool .= "0123456789";
    $pool .= "QWERTZUIOPASDFGHJKLYXCVBNM";
    $pool .= "!§$%&/()=?*+#-:;<>";
	$pass_word ="";
    $pool2 = str_shuffle($pool);
	srand ((double)microtime()*1000000);
	for($index = 0; $index < 8; $index++){
		$pass_word .= substr($pool2,(rand()%(strlen ($pool2))), 1);
	}
    echo $pass_word;
	/* Variante Ende
	*/
	$message = "Hallo " . $result['display_name'] . ',<br />';
	$message .= "Dein Passwort für den internen Bereich wurde soeben zurückgesetzt und lautet ab sofort: <b>";
	$message .= $pass_word . '</b> <br />';
    $message .= "Du kannst es im internen Bereich jederzeit wieder ändern.";
    $message .= "<br /><br />Gut Spiel<br />Spielmannszug Südlohn 1950 e.V.";
	$pass_word = md5($pass_word);
	       $result = $wpdb->update($wpdb->prefix . 'users', array(
		   'user_pass'	=> ''.$pass_word.''),
		   array(
			'user_email' => $_POST['jbs_user_email']),
				array('%s'), 
				array('%s'));

			if(false !== $result){
				jbs_success()->add('success', __('Dein Passwort wurde zurückgesetzt. Du bekommst eine Mail von uns.'));
				$headers = array('Content-Type: text/html; charset=UTF-8');
				$t = wp_mail($mail,'Dein Passwort wurde zurückgesetzt',$message, $headers);
			}
        }
//update Datenbank

    }
}

add_action('init', 'jbs_login_member');

// register a new user
function jbs_add_new_member() {
    if (isset( $_POST['jbs_register_nonce'] )  && wp_verify_nonce( $_REQUEST['jbs_register_nonce'], 'jbs_register_action' ) ) {
        $user_login = '';
        $user_email = "";
        $user_first = "";
        $user_last = "";
        $user_pass = "";
        $pass_confirm = "";
        $instrument = "";
        $strasse = "";
        $hausnr = "";
        $plz = "";
        $ort = "";
        $mobilnumber = "";
        if( get_option('register-username') == 'on' ){$user_login = $_POST["jbs_user_login"];}
        if( get_option('register-email') == 'on' ){$user_email = $_POST["jbs_user_email"];}
        if( get_option('register-first-name') == 'on' ){$user_first = $_POST["jbs_user_first"];}
        if( get_option('register-last-name') == 'on' ){$user_last = $_POST["jbs_user_last"];}
        if( get_option('register-password') == 'on' ){$user_pass = $_POST["jbs_user_pass"];}
        if( get_option('register-password2') == 'on' ){$pass_confirm = $_POST["jbs_user_pass_confirm"];}
        if( get_option('register-instrument') == 'on' ){$instrument = $_POST["instrument"];}
        if( get_option('register-street') == 'on' ){$strasse = $_POST["jbs_user_street"];}
        if( get_option('register-nr') == 'on' ){$hausnr = $_POST["jbs_user_street_nr"];}
        if( get_option('register-plz') == 'on' ){$plz = $_POST["jbs_user_plz"];}
        if( get_option('register-ort') == 'on' ){$ort = $_POST["jbs_user_ort"];}
        if( get_option('register-mobil') == 'on' ){$mobilnumber = $_POST["jbs_user_mobil"];}
        
        $gruppe = get_option('standard-gruppe');

// this is required for username checks
        require_once(ABSPATH . WPINC . '/registration.php');

        if (username_exists($user_login) && get_option('register-username') == 'on') {
// Username already registered
            jbs_errors()->add('username_unavailable', __('Benutzername bereits vergeben'));
        }
        if (!validate_username($user_login) && get_option('register-username') == 'on') {
// invalid username
            jbs_errors()->add('username_invalid', __('Ungültiger Benutzername'));
        }
        if ($user_login == '' && get_option('register-username') == 'on') {
// empty username
            jbs_errors()->add('username_empty', __('Kein Benutzername angegeben'));
        }
        if (!is_email($user_email) && get_option('register-email') == 'on') {
//invalid email
            jbs_errors()->add('email_invalid', __('Ungültige E-Mail Adresse'));
        }
        if (email_exists($user_email) && get_option('register-email') == 'on') {
//Email address already registered
            jbs_errors()->add('email_used', __('E-Mail Adresse bereits in Verwendung'));
        }
        if ($user_pass == '' && get_option('register-password') == 'on') {
// passwords do not match
            jbs_errors()->add('password_empty', __('Kein Benutzername eingegeben'));
        }
        if ($user_pass != $pass_confirm && get_option('register-password') == 'on' && get_option('register-password2') == 'on') {
// passwords do not match
            jbs_errors()->add('password_mismatch', __('Passwörter stimmen nicht überein'));
        }
        //blacklist_array erstellen
        $blacklist = "";
        $list = get_option('register-blacklist');
        if( !empty($list) ){
            $domains = explode(',', $list);
        }
        foreach ($domains as $domain) {
            $blacklist[] = strtolower($domain);
        }
        if ( is_email($user_email) ) {
            $parts = explode('@', $user_email);
            $parts = explode('.', $parts[count($parts)-1]);
            $domain = $parts[count($parts)-1];
            if ( in_array(strtolower($domain), $blacklist) ) {
                jbs_errors()->add('email_domain_blacklisted', __('Die E-Mail Domain ist nicht zugelassen', 'jbs-events'));
            }
        }

        $errors = jbs_errors()->get_error_messages();

// only create the user in if there are no errors
        if (empty($errors)) {

            $new_user_id = wp_insert_user(array(
                'user_login' => $user_login,
                'user_pass' => $user_pass,
                'user_email' => $user_email,
                'first_name' => $user_first,
                'last_name' => $user_last,
                'user_registered' => date('Y-m-d H:i:s'),
                'role' => 'subscriber'
                    )
            );

//custom meta hinzufügen
            add_user_meta($new_user_id, 'instrument', $instrument);
            add_user_meta($new_user_id, 'gruppe', $gruppe);
            add_user_meta($new_user_id, 'mobil', $mobilnumber);
            add_user_meta($new_user_id, 'strasse', $strasse);
            add_user_meta($new_user_id, 'hausnr', $hausnr);
            add_user_meta($new_user_id, 'plz', $plz);
            add_user_meta($new_user_id, 'ort', $ort);
            add_user_meta($new_user_id, 'approval_status', 0);

            if ($new_user_id) {

            // Admin Nachricht
            $message = __('Es hat sich ein neuer Benutzer registriert.') . "\r\n\r\n";
            $message .= sprintf(__('Benutzername: %s'), $user_login) . "\r\n";
            $message .= sprintf(__('E-Mail: %s'), $user_email) . "\r\n\r\n";
            $message .= __('Der Benutzer muss noch freigeschaltet werden.') . "\r\n";
            $message .= sprintf(__('Link zum Admin-Panel:  %s'), wp_login_url()) . "\r\n";

            if(get_option('register-new-admin') == 'on'){
                wp_mail(get_option('admin_email'), sprintf(__('[%s] Neuer Benutzer registriert'), get_option('blogname')), $message);
                wp_mail('jakobbennemann@hotmail.de', sprintf(__('[%s] Neuer Benutzer registriert'), get_option('blogname')), $message);
            }


            // Benutzer Nachricht
            $m = sprintf(__('Hallo %s %s,'), $user_first, $user_last) . "\r\n\r\n";
            $m .= __("Danke für deine Registrierung.") . "\n";
            $m .= __("Nach einer Prüfung deiner Daten wirst du freigeschaltet. ");
            $m .= __("Sobald wir dich freigeschaltet haben erhältst du von uns eine E-Mail.") . "\n";
            $m .= __("Mit folgenden Benutzerinfos kannst du dich dann anmelden.") . "\n\r\n";
            $m .= sprintf(__('Benutzername: %s'), $user_login) . "\r\n";
            $m .= sprintf(__('Passwort: %s'), $user_pass) . "\r\n\r\n";
            $m .= __('Bei Fragen kannst du einfach auf diese E-Mail antworten.') . "\r\n\r\n";
            $m .= __('"Gut Spiel"') . "\r\n";
            $m .= __('Der Vorstand') . "\r\n";

            if(get_option('register-new-user') == 'on'){
                wp_mail($user_email, sprintf(__('[%s] Dein Benutzername und Passwort'), get_option('blogname')), $m);
            }

// send an email to the admin alerting them of the registration
                //wp_new_user_notification($new_user_id, $user_pass);
                $time = date("Y-m-d H:i:s");
                insertLogDB('Registration Mail sent', $time, 'mail:' . $user_email, $new_user_id, $user_first . ' ' . $user_last, $_SERVER['REMOTE_ADDR']);

                $time = date("Y-m-d H:i:s");
                insertLogDB('New Registration', $time, 'user-id:' . $new_user_id . '|first:' . $user_first . '|last:' . $user_last . '|email:' . $user_email . '|gruppe:' . $gruppe .'|instr:' . $instrument . '|mobil:' . $mobilnumber . '|strasse:' . $strasse . '|hausnr:' . $hausnr . '|plz:' . $plz . '|ort:' . $ort . 'approval:0', $new_user_id, $user_first . ' ' . $user_last, $_SERVER['REMOTE_ADDR']);

                jbs_success()->add('registered-new', __('Vielen Dank für Deine Registrierung!','jbs-events'));
            }
        }
    }
}

add_action('init', 'jbs_add_new_member');

if (!function_exists('wp_new_user_notification')) {

    function wp_new_user_notification($user_id, $plaintext_pass = '') {
        $user = new WP_User($user_id);

        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);
        $user_name = stripslashes($user->display_name);

       // if (empty($plaintext_pass))
       //     return;

        $message = NULL;

    }

}


// used for tracking error messages
function jbs_errors() {
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}
// used for tracking error messages
function jbs_success() {
    static $wp_success; // Will hold global variable safely
    return isset($wp_success) ? $wp_success : ($wp_success = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function jbs_show_error_messages() {
    if ($codes = jbs_errors()->get_error_codes()) {
        echo '<div class="jbs_errors">';
        // Loop error codes and display errors
        foreach ($codes as $code) {
            $message = jbs_errors()->get_error_message($code);
            echo '<span class="error"><strong>' . __('Fehler') . '</strong>: ' . $message . '</span><br/>';
        }
        echo '</div>';
    }
}
// displays error messages from form submissions
function jbs_show_success_messages() {
    if ($codes = jbs_success()->get_error_codes()) {
        echo '<div class="jbs_errors">';
        // Loop error codes and display errors
        foreach ($codes as $code) {
            $message = jbs_success()->get_error_message($code);
            echo '<span class="update"><strong>' . __('Erfolg') . '</strong>: ' . $message . '</span><br/>';
        }
        echo '</div>';
    }
}

add_action('show_user_profile', 'jbs_extra_user_profile_fields');
add_action('edit_user_profile', 'jbs_extra_user_profile_fields');

function jbs_extra_user_profile_fields($user) {
    ?>
    <h3><?php _e("Interne Infos Spielmannszug", "blank"); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="instrument"><?php _e("Instrument"); ?></label></th>
            <td>
                <?php
                if (esc_attr(get_the_author_meta('instrument', $user->ID)) == 'Flöte') {
                    $fl = "selected";
                    $schl = "";
                } else if (esc_attr(get_the_author_meta('instrument', $user->ID)) == 'Schlagwerk') {
                    $fl = "";
                    $schl = "selected";
                }
                ?>
                <select name="instrument" value="<?php echo esc_attr(get_the_author_meta('instrument', $user->ID)); ?>" required>
                    <option value="Flöte" <?php echo $fl; ?> >Flöte</option>
                    <option value="Schlagwerk"<?php echo $schl; ?> >Schlagwerk</option>
                </select><br />
                <span class="description"><?php _e("Welches Instrument spielst du?"); ?></span>
            </td>
        </tr>
        <?php if(current_user_can('administrator') && is_admin()){ ?>
        <tr>
            <th><label for="gruppe"><?php _e("Mitgliedergruppe"); ?></label></th>
            <td>
                <?php
                if (esc_attr(get_the_author_meta('gruppe', $user->ID)) == 'Aktives Mitglied') {
                    $akt = "selected";
                    $passiv = "";
                    $ehren = "";
                    $sonst = "";
                } else if (esc_attr(get_the_author_meta('gruppe', $user->ID)) == 'Passives Mitglied') {
                    $akt = "";
                    $passiv = "selected";
                    $ehren = "";
                    $sonst = "";
                } else if (esc_attr(get_the_author_meta('gruppe', $user->ID)) == 'Ehrenmitglied') {
                    $akt = "";
                    $passiv = "";
                    $ehren = "selected";
                    $sonst = "";
                } else if (esc_attr(get_the_author_meta('gruppe', $user->ID)) == 'Sonstiges Mitglied') {
                    $akt = "";
                    $passiv = "";
                    $ehren = "";
                    $sonst = "selected";
                }
                ?>
                <select name="gruppe" required>
                    <option value="Aktives Mitglied" <?php echo $akt; ?> >Aktives Mitglied</option>
                    <option value="Passives Mitglied"<?php echo $passiv; ?> >Passives Mitglied</option>
                    <option value="Ehrenmitglied"<?php echo $ehren; ?> >Ehrenmitglied</option>
                    <option value="Sonstiges Mitglied"<?php echo $sonst; ?> >Sonstiges Mitglied</option>
                </select><br />
                <span class="description"><?php _e("Welchen Mitgliedsstatus hast du im Verein?"); ?></span>
            </td>
        </tr>
        <?php ;} ?>
        <tr>
            <th><label for="mobil"><?php _e("Handynummer"); ?></label></th>
            <td>
                <input type="text" name="mobil" id="mobil" class="regular-text" 
                       value="<?php echo esc_attr(get_the_author_meta('mobil', $user->ID)); ?>" /><br />
                <span class="description"><?php _e("Gibt deine Handynummer ein."); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="adresse"><?php _e("Straße, Hausnummer"); ?></label></th>
            <td>
                <input type="text" name="strasse" id="strasse" class="" 
                       value="<?php echo esc_attr(get_the_author_meta('strasse', $user->ID)); ?>" />
                <input type="text" name="hausnr" id="hausnr" class="" 
                       value="<?php echo esc_attr(get_the_author_meta('hausnr', $user->ID)); ?>" /><br />
                <span class="description"><?php _e("Straße, "); ?></span>
                <span class="description"><?php _e("Hausnummer"); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="adresse2"><?php _e("PLZ, Ort"); ?></label></th>
            <td>
                <input type="text" name="plz" id="plz" class="" 
                       value="<?php echo esc_attr(get_the_author_meta('plz', $user->ID)); ?>" />
                <input type="text" name="ort" id="ort" class="" 
                       value="<?php echo esc_attr(get_the_author_meta('ort', $user->ID)); ?>" /><br />
                <span class="description"><?php _e("PLZ, "); ?></span>
                <span class="description"><?php _e("Ort"); ?></span>
            </td>
        </tr>
        <?php
                if(current_user_can('administrator') && is_admin()){

        ?>
        <tr>
            <th><label for="activate"><?php _e("Benutzer aktivieren"); ?></label></th>
            <td>
                <?php
                $ch = "";
                if (esc_attr(get_the_author_meta('approval_status', $user->ID)) == 1) {
                    $ch = "checked";
                }

                ?>
                <input type="checkbox" name="activate" id="activate" 
                       <?php echo $ch; ?> /><br />
                <span class="description"><?php _e("Ist der Benutzer aktiviert?"); ?></span>
            </td>
        </tr>
        <?php ;} ?>
    </table>
    <?php
}

add_action('personal_options_update', 'jbs_save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'jbs_save_extra_user_profile_fields');

function jbs_save_extra_user_profile_fields($user_id) {
    $saved = false;
    if (current_user_can('edit_user', $user_id)) {
        update_user_meta($user_id, 'mobil', $_POST['mobil']);
        update_user_meta($user_id, 'instrument', $_POST['instrument']);
        update_user_meta($user_id, 'gruppe', $_POST['gruppe']);
        update_user_meta($user_id, 'strasse', $_POST['strasse']);
        update_user_meta($user_id, 'hausnr', $_POST['hausnr']);
        update_user_meta($user_id, 'plz', $_POST['plz']);
        update_user_meta($user_id, 'ort', $_POST['ort']);
        if ($_POST['activate'] != "") {
            update_user_meta($user_id, 'approval_status', 1);
            $time = date("Y-m-d H:i:s");
            $nutzer = get_userdata($user_id);
            insertLogDB('Profile (Termine) Updated', $time, 'user-id:' . $user_id . '|gruppe:' . $_POST['gruppe'] . '|instr:' . $_POST['instrument'] . '|mobil:' . $_POST['mobil'] . '|strasse:' . $_POST['strasse'] . '|hausnr:' . $_POST['hausnr'] . '|plz:' . $_POST['plz'] . '|ort:' . $_POST['ort'] . 'approval:1', $user_id, $nutzer->display_name, $_SERVER['REMOTE_ADDR']);
        } else {
            $time = date("Y-m-d H:i:s");
            $nutzer = get_userdata($user_id);
            insertLogDB('Profile (Termine) Updated', $time, 'user-id:' . $user_id . '|gruppe:' . $_POST['gruppe'] . '|instr:' . $_POST['instrument'] . '|mobil:' . $_POST['mobil'] . '|strasse:' . $_POST['strasse'] . '|hausnr:' . $_POST['hausnr'] . '|plz:' . $_POST['plz'] . '|ort:' . $_POST['ort'] . 'approval:0', $user_id, $nutzer->display_name, $_SERVER['REMOTE_ADDR']);

            update_user_meta($user_id, 'approval_status', 0);
        }
        $saved = true;
    }
    return true;
}

function jbs_backend_profile_action_link($actions, $user_object) {
    if (get_user_meta($user_object->ID, 'approval_status', true) == 1 && $user_object->ID != 1) {

        $dir = plugins_url('includes/jbs-registration.php?user_id=' . $user_object->ID . '&action=deactivate', dirname(__FILE__));
        $actions['view profile'] = "<a class='view_frontend_profile' href='" . $dir . "'>" . __('Deaktivieren', 'jbs-events') . "</a>";
    } else {
        if ($user_object->ID != 1) {
            $dir = plugins_url('includes/jbs-registration.php?user_id=' . $user_object->ID . '&action=activate', dirname(__FILE__));
            $actions['view profile'] = "<a class='view_frontend_profile' href='" . $dir . "'>" . __('Aktivieren', 'jbs-events') . "</a>";
        }
    }


    return $actions;
}

add_filter('user_row_actions', 'jbs_backend_profile_action_link', 10, 2);

?>
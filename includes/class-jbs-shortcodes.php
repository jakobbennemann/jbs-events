<?php
require_once (ABSPATH . WPINC . '/pluggable.php');
require_once (ABSPATH . 'wp-content/plugins/jbs-events/jbs-events.php');
include_once(dirname(__FILE__) . '/jbs-registration.php');
if( ! class_exists( 'TCPDF' ) ){
require_once (ABSPATH . 'wp-content/plugins/jbs-events/tcpdf/tcpdf.php');
}
class MYPDF extends TCPDF {

    // Load table data from file
    public function LoadData($file) {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach($lines as $line) {
            $data[] = explode(';', chop($line));
        }
        return $data;
    }

    // Colored table
    public function ColoredTable($header,$data) {
        // Colors, line width and bold font
        $this->SetFillColor(252, 252, 252);
        $this->SetTextColor(51);
        //$this->SetDrawColor(51);
        $this->SetLineWidth(0.1);
        $this->SetFont('', 'B');
        // Header
        $w = array(10, 50, 30, 50, 30);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 6, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        $nr = 1;
        foreach($data as $row) {
            $this->Cell($w[0], 8, $nr, 'LR', 0, 'L', $fill);
            $nr+=1;
            $this->Cell($w[1], 8, $row['name'], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 8, $row['vname'], 'LR', 0, 'L', $fill);
            $this->Cell($w[3], 8, $row['info'], 'LR', 0, 'L', $fill);
            $this->Cell($w[4], 8, $row['instrument'], 'LR', 0, 'C', $fill);
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

function insertLogInDB($action, $time, $info, $id, $name, $ip) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_log';
    $wpdb->insert($table, array(
        'time' => $time,
        'action' => $action,
        'info' => $info,
        'userid' => $id,
        'user' => $name,
        'ip' => $ip
            ), array(
        '%s', '%s', '%s', '%d', '%s', '%s'
    ));
}

function get_EventdescriptionFrontend($event_id) {
    global $wpdb;
    $db_table = $wpdb->prefix . 'jbs_events';

    //Abfrage
    $abfrage = $wpdb->get_row("SELECT description FROM " . $db_table . " WHERE id =" . $event_id . "");
    return $abfrage->description;
}

if(isset($_POST['submit_pdf'])){
    // PDF drucken
    $e_id = $_POST['eventid'];
        global $wpdb;
        $table_name = $wpdb->prefix . "jbs_events_participation";
        $table_events = $wpdb->prefix . "jbs_events";
        $eventname = $wpdb->get_var("SELECT description FROM $table_events WHERE id=$e_id;");
        $eventdate = $wpdb->get_var("SELECT date FROM $table_events WHERE id=$e_id;");
        //Alle Benutzer hoel, die für das event zugesagt haben.
        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE eventid=$e_id;");
        foreach ($result as $row) {
            $meta = $wpdb->prefix . "usermeta";
            $instrument = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='instrument' && user_id=$row->userid");
            $fname = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='first_name' && user_id=$row->userid");
            $lname = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='last_name' && user_id=$row->userid");
            $info = $row->info_participant;
            $id = $row->userid;

            if($row->status == 0){
                $teilnehmerIds[] = array(
                    'id'  => $id,
                    'name'  => $lname,
                    'vname'  => $fname,
                    'instrument'    =>  $instrument,
                    'info'  =>  $info);
            }
            if($row->status == 1){
                $unsicherIds[] = array(
                    'id'  => $id,
                    'name'  => $lname,
                    'vname'  => $fname,
                    'instrument'    =>  $instrument,
                    'info'  =>  $info);
            }
            if($row->status == 2){
                $absageIds[] = array(
                    'id'  => $id,
                    'name'  => $lname,
                    'vname'  => $fname,
                    'instrument'    =>  $instrument,
                    'info'  =>  $info);
            }
        }


        //prepare $html
        $html = '<h4 style="font-weight:normal;padding-bottom:20px;">Zusagen:</h4>';
        /*$html .= '<table id="jbs-pdf"><tr><th style="width:3%;"></th><th style="width:30%;">Name</th><th style="width:25%;">Vorname</th><th style="width:30%;">Information</th><th style="width:15%;">Instrument</th></tr>';
        $nr = 1;
        foreach ($teilnehmerIds as $t) {
            $html .= '<tr style="font-size:12px;">';
                $html .= '<td style="width:3%;">';
                $html .= ''. $nr;
                $nr += 1;
                $html .= '</td>';
                $html .= '<td style="width:30%;">';
                $html .= $t['name'];
                $html .= '</td>';
                $html .= '<td style="width:25%;">';
                $html .= $t['vname'];
                $html .= '</td>';
                $html .= '<td style="width:30%;">';
                $html .= $t['info'];
                $html .= '</td>';
                $html .= '<td style="width:15%;">';
                $html .= $t['instrument'];
                $html .= '</td>';
            $html .= '</tr>';
        }*/

        /*echo "<h1>" .count($teilnehmerIds). "</h1>";
        echo "<h1>" .count($$unsicherIds). "</h1>";
        echo "<h1>" .count($absageIds). "</h1>";
        echo "<h1>" .$absageIds[0]['id']. "</h1>";
        echo "<h1>" .$absageIds[0]['name']. "</h1>";
        echo "<h1>" .$absageIds[0]['info']. "</h1>";
        echo "<h1>" .$absageIds[0]['instrument']. "</h1>";*/

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$tage = array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
// set document information
$pdf->SetCreator("Jakob Bennemann");
$pdf->SetAuthor('Spielmannszug Südlohn 1950 e.V.');
$pdf->SetTitle('Anwesenheitsliste');
$pdf->SetSubject('');
$date = new DateTime($eventdate);

$tag = $date->format('w');
$tag = $tage[$tag] . ', ';


// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, $eventname . ' | ' . $tag. $date->format('d.m.Y'));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

// column titles
$header = array('', 'Name', 'Vorname', 'Information', 'Instrument');

// write the html Data

if(count($teilnehmerIds) > 0){
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->ColoredTable($header, $teilnehmerIds);

}

// print colored table


if(count($absageIds) > 0){
            $pdf->Ln();
            $pdf->Ln();

$html = '<h4 style="font-weight:normal;padding-bottom:20px;">Absagen:</h4>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->ColoredTable($header, $absageIds);

}

if(count($unsicherIds) > 0){
            $pdf->Ln();
            $pdf->Ln();

$html = '<h4 style="font-weight:normal;padding-bottom:20px;">Unsicher:</h4>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->ColoredTable($header, $unsicherIds);}
// ---------------------------------------------------------

if( count($teilnehmerIds) <= 0 && count($unsicherIds) <= 0 && count($absageIds) <= 0 ){

    //keine Infos vorhanden
    $html = '<h1 style="text-align:center;font-style:italic;font-size:15px;color:#990000;">Es sind keine Informationen vorhanden!</h1>';
$pdf->writeHTML($html, true, false, true, false, '');
}

// close and output PDF document
$pdf->Output($eventname . '.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+



}

if( isset($_POST['submit_pdf'])){
    //log download
    global $current_user;
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    insertLogInDB('PDF downloaded '  , $time, 'event:' . get_EventdescriptionFrontend($_POST['eventid']), -1 ,$current_user->display_name, $_SERVER['REMOTE_ADDR']);

    //weitere Tests machen
}


if (isset($_POST['submit_teilnehmen']) || isset($_POST['submit_unsicher']) || isset($_POST['submit_absagen'])) {

    $userid = $_POST['userid'];
    $eventid = $_POST['eventid'];
    $status = '';
    $info_participant = '';

    if (isset($_POST['info_participant'])) {
        $info_participant = $_POST['info_participant'];
    }

    if (isset($_POST['submit_teilnehmen'])) {
        $status = '0';
    }
    if (isset($_POST['submit_unsicher'])) {
        $status = '1';
    }
    if (isset($_POST['submit_absagen'])) {
        $status = '2';
    }

    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';

    $vorhanden = $wpdb->get_row("SELECT * FROM $table WHERE eventid=$eventid AND userid=$userid;");
    if ($vorhanden != null) {
        //update
        $wpdb->update($table, array('userid' => $userid, 'eventid' => $eventid, 'info_participant' => $info_participant, 'status' => $status), array('userid' => $userid, 'eventid' => $eventid), array('%d', '%d', '%s', '%d'), array('%d', '%d'));
    } else {
        //einfügen
        $wpdb->insert($table, array('userid' => $userid, 'eventid' => $eventid, 'info_participant' => $info_participant, 'status' => $status), array('%d', '%d', '%s', '%d'));
    }
    global $current_user;
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    insertLogInDB('Participation changed', $time, 'eventid:' . $eventid . '|info.part:' . $info_participant . '|status:' . $status, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
    wp_redirect($_SERVER['REQUEST_URI']);
    exit;
}

function jbs_display_adresses(){
	$name = "";
	$mobil = "";
	$strasse = "";
	$hnr = "";
	$geb = "";
	$plz = "";
	$ort = "";
    if(is_user_logged_in()){
        ?>
        <div class="wrap">
            <h2>Adressen</h2>
            <table id="jbs_adresses">
                <tr>
                    <th><?php _e('Name', 'jbs-events'); ?></th>
                    <th><?php _e('Vorname', 'jbs-events'); ?></th>
                    <th><?php _e('Adresse', 'jbs-events'); ?></th>
                    <th><?php _e('Handynummer / E-Mail', 'jbs-events'); ?></th>
                </tr>
                <?php 
                    global $wpdb;
                    $table_name = $wpdb->prefix . "users";
                    $table_usermeta = $wpdb->prefix . "usermeta";
                    $results = $wpdb->get_results("SELECT ID,user_email FROM $table_name ORDER BY display_name ASC;");
                    $count = 0;
                    foreach ($results as $r) {
                        $ids[] = $r->ID;
                    }

                    foreach ($ids as $t) {
                        $firstname = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='first_name';");
                        $lastname = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='last_name';");
                        $strasse = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='strasse';");
                        $hausnr = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='hausnr';");
                        $plz = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='plz';");
                        $ort = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='ort';");
                        $mobil = $wpdb->get_var("SELECT meta_value FROM $table_usermeta WHERE user_id=$t && meta_key='mobil';");
                        $email = $wpdb->get_var("SELECT user_email FROM $table_name WHERE ID=$t;");
                        $meta_infos[] = array(
                            'id' => $t,
                            'first_name' => $firstname,
                            'last_name' =>  $lastname,
                            'strasse'   =>  $strasse,
                            'hausnr'    =>  $hausnr,
                            'plz'   =>  $plz,
                            'ort'   =>  $ort,
                            'email' =>  $email,
                            'mobil' =>  $mobil,
                            'approval_status' => get_user_meta($t, 'approval_status', true));
                    }
                    foreach ($meta_infos as $key => $inhalt) {
                        $l[$key] = $inhalt['last_name'];
                        $f[$key] = $inhalt['first_name'];
                        $p[$key] = $inhalt['plz'];
                    }

                    array_multisort($l, SORT_ASC, $meta_infos);
                    foreach ($meta_infos as $eintrag) {
                        if($eintrag['approval_status'] != 1)continue;
                        echo '<tr>';
                            echo '<td>';
                                echo $eintrag['last_name'];
                            echo '</td>';
                            echo '<td>';
                                echo $eintrag['first_name'];
                            echo '</td>';
                            echo '<td>';
                                echo $eintrag['strasse'] . ' ' . $eintrag['hausnr'] . '<br />';
                                echo $eintrag['plz'] . ' ' . $eintrag['ort'];
                            echo '</td>';
                            echo '<td>';
                                echo $eintrag['mobil'] . '<br />' . $eintrag['email'];
                            echo '</td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </div>

        <?php
    }
	
}

function jbs_display_events($atts) {
    $y = '';
    $g = '';
    extract(shortcode_atts(array(
        'y' => '',
        'g' => ''
                    ), $atts
            )
    );

    global $wpdb;
    $table_name = $wpdb->prefix . 'jbs_events';
    $table_name2 = $wpdb->prefix . 'jbs_events_participation';

        
    if (is_user_logged_in()) {
        global $current_user;
        wp_get_current_user();
        $results = $wpdb->get_results("SELECT * FROM $table_name ORDER BY date ASC;");
        $eventsToShow = 0;
        foreach ($results as $key) {
            $year = date('Y', strtotime($key->date));
            
            if ($y == $year) {$eventsToShow++;}
        }
        if($eventsToShow > 0 && $y != '' || $eventsToShow == 0 && $y == ''){
            $table = "<table id='jbs-events'>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th style='width:14%;' class='date'>".__('Datum','jbs-events')."</th>";
            $table .= "<th class='descr'>".__('Termin','jbs-events')."</th>";
            $table .= "<th class='time_start'>".__('Uhrzeit','jbs-events')."</th>";
            $table .= "<th class='time_end'>".__('Ende','jbs-events')."</th>";
            $table .= "<th class='info'>".__('Info','jbs-events')."</th>";
            $table .= "</tr>";
            $table .= "</thead>";
        }
        $gerade = 0;

        $table .= "<tbody>";
        foreach ($results AS $r) {
            $year = date('Y', strtotime($r->date));
            $month = date('m', strtotime($r->date));
            $day = date('d', strtotime($r->date));
            

            $test_datum = $r->date;
            $wochentage = array('So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa');
            list ($jahr, $monat, $tag) = explode('-', $test_datum);
            $datum = getdate(mktime(0, 0, 0, $monat, $tag , $jahr));
            $wochentag = $datum['wday'];

            if ($y == '' || $y == $year) {
                if ($gerade % 2 == 0) {
                    $table .= "<tr class='even'>";
                } else {
                    $table .= "<tr class='odd'>";
                }
                $table .= "<td class='date'>$wochentage[$wochentag], $day.$month.$year</td>";
                $table .= "<td class='descr'>";
                if ($r->link != "" && $r->link != null) {
                    $table .= "<a href=" . $r->link . ">";
                }
                $table .= "$r->description";
                if ($r->link != "" && $r->link != null) {
                    $table .= "</a>";
                }
                $end = "";
                if($r->time_end != ""){$end = "ca. " . $r->time_end;}else{}
                $table .= "</td>";
                $table .= "<td class='time_start'>$r->time_start</td>";
                $table .= "<td class='time_end'>$end</td>";
                $table .= "<td class='info'>$r->extra_info</td>";
                $table .= "</tr>";

                $table .= "<form method='POST'>";


                $gruppe = esc_attr(get_user_option('gruppe', $current_user->ID));
                $tableUserMeta = $wpdb->prefix . 'usermeta';
                $info_teilnahme = "";
                $abfrage = $wpdb->get_row("SELECT * FROM $tableUserMeta WHERE meta_key = 'gruppe' AND user_id= $current_user->ID");
                $gruppe_user = $abfrage->meta_value;
                $abfrage = $wpdb->get_row("SELECT * FROM $table_name WHERE id=$r->id");
                $gruppenzugehoerigkeit = array();
                if ($abfrage->group_aktiv == 1) {
                    array_push($gruppenzugehoerigkeit, 'Aktives Mitglied');
                }
                if ($abfrage->group_passiv == 1) {
                    array_push($gruppenzugehoerigkeit, 'Passives Mitglied');
                }
                if ($abfrage->group_ehren == 1) {
                    array_push($gruppenzugehoerigkeit, 'Ehrenmitglied');
                }
                if ($abfrage->group_sonstige == 1) {
                    array_push($gruppenzugehoerigkeit, 'Sonstiges Mitglied');
                }

                if($r->participation == 1 || $r->info_participant == 1){
                    $today = date("Y-m-d H:i:s");

                    if ($r->date < $today) {
                        $gerade--;
                        continue;
                    }
                }

                if ($r->participation == 1 && $r->info_participant == 1) {

                    if(in_array($gruppe_user, $gruppenzugehoerigkeit)){


                    if ($gerade % 2 == 0) {
                        $table .= "<tr class='even info'>";
                    } else {
                        $table .= "<tr class='odd info'>";
                    }
                    $data = $wpdb->get_row("SELECT * FROM $table_name2 WHERE eventid=$r->id AND userid=$current_user->ID;");

                    if ($data->info_participant != null) {
                        $table .= "<td colspan=5><input type='text' name='info_participant' value='" . $data->info_participant . "' /></td></tr>";
                    } else {
                        $table .= "<td colspan=5><input type='text' name='info_participant' placeholder='Teilnahmeinfo für \"".get_EventdescriptionFrontend($r->id)."\" angeben...' /></td></tr>";
                    }
                    }
                }
                if ($r->participation == 1) {

                    if (in_array($gruppe_user, $gruppenzugehoerigkeit)) {

                        if ($gerade % 2 == 0) {
                            $table .= "<tr class='even teilnahme'>";
                        } else {
                            $table .= "<tr class='odd teilnahme'>";
                        }

                        $data = $wpdb->get_row("SELECT * FROM $table_name2 WHERE eventid=$r->id AND userid=$current_user->ID;");
                        $teilnahmeinfo = '';
                        if ($data != null) {
                            if ($data->status == 0) {
                                $teilnahmeinfo = __('Du nimmst teil.','jbs-events');
                            }
                            if ($data->status == 1) {
                                $teilnahmeinfo = __('Du bist noch nicht sicher.','jbs-events');
                            }
                            if ($data->status == 2) {
                                $teilnahmeinfo = __('Du nimmst nicht teil.','jbs-events');
                            }
                        } else {
                            $teilnahmeinfo = __('<-- Bitte auswählen','jbs-events');
                        }

                        if(current_user_can('editor') || current_user_can('administrator')){
                            $table .= "<td colspan=4><input type='hidden' value='$current_user->ID' name='userid' /><input type='hidden' value='$r->id' name='eventid' /><input type='submit' title='". __('teilnehmen', 'jbs-events') ."' name='submit_teilnehmen' class='icon-ok' id='sub_teilnehmen' value='&#xf00c;' />  <input type='submit' class='icon-minus' name='submit_unsicher' title='". __('unsicher', 'jbs-events') ."' value='&#xf068;'>  <input type='submit' class='icon-remove' title='". __('absagen', 'jbs-events') . "' name='submit_absagen' value='&#xf00d;'>  <span>" . $teilnahmeinfo . "</span></td><td><input type='submit' class='icon-file' name='submit_pdf' value='.pdf Download' /></td></tr>";
                        }else{
                            $table .= "<td colspan=5><input type='hidden' value='$current_user->ID' name='userid' /><input type='hidden' value='$r->id' name='eventid' /><input type='submit' title='". __('teilnehmen', 'jbs-events') ."' name='submit_teilnehmen' id='sub_teilnehmen'  class='icon-ok' value='&#xf00c;' />  <input type='submit' class='icon-minus' name='submit_unsicher' title='". __('unsicher', 'jbs-events') ."' value='&#xf068;'>  <input type='submit' class='icon-remove' title='". __('absagen', 'jbs-events') . "' name='submit_absagen' value='&#xf00d;'>  <span>" . $teilnahmeinfo . "</span></td></tr>";
                        }

                    }
                }
                $table .= "</form>";
                $gerade++;
            }
        }
        if($eventsToShow > 0 && $y != '' || $eventsToShow == 0 && $y == ''){

            $table .= "</tbody>";
            $table .= "</table>";
        }else{
            $table = "<p class='no-events'>Es sind keine Termine vorhanden.</p>";
        }
        return $table;
    } else {
        $results = $wpdb->get_results("SELECT *FROM $table_name ORDER BY date ASC;");


        $table = "<table id='jbs-events'>";
        $table .= "<thead>";
        $table .= "<tr>";
        $table .= "<th>".__('Datum','jbs-events')."</th>";
        $table .= "<th>".__('Termin','jbs-events')."</th>";
        $table .= "</tr>";
        $table .= "</thead>";
        $table .= "<tbody>";

        $gerade = 0;
        foreach ($results AS $r) {
            if ($gerade % 2 == 0) {
                $table .= "<tr class='even'>";
            } else {
                $table .= "<tr class='odd'>";
            }
            $year = date('Y', strtotime($r->date));
            $month = date('m', strtotime($r->date));
            $day = date('d', strtotime($r->date));

            $test_datum = $r->date;
            $wochentage = array('So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa');
            list ($jahr, $monat, $tag) = explode('-', $test_datum);
            $datum = getdate(mktime(0, 0, 0, $monat, $tag, $jahr));
            $wochentag = $datum['wday'];
            if ($y == $year || $y == '') {
                $table .= "<td>";
                $table .= "$wochentage[$wochentag], $day.$month.$year</td>";
                $table .= "<td>";
                if ($r->link != "" && $r->link != null) {
                    $table .= "<a href=" . $r->link . ">";
                }
                $table .= "$r->description";
                if ($r->link != "" && $r->link != null) {
                    $table .= "</a>";
                }
                $table .= "</td>";
                $table .= "</tr>";
                $gerade++;
            }
        }
        $table .= "</tbody>";
        $table .= "</table>";
        return $table;
    }
}

add_shortcode('jbs_events', 'jbs_display_events');
add_shortcode('jbs_addresses', 'jbs_display_adresses');
add_shortcode('jbs_knobelturnier', 'jbs_display_knobelturnier_form');

function jbs_display_knobelturnier_form(){
    ob_start();

    if(get_option('knobelturnier-anmeldungen-aktiviert') == '' || get_option('knobelturnier-anmeldungen-aktiviert') == 'off'){ ?>
        <div class="wrap">
            <?php 
                $text = get_option('knobelturniermessagetext');
            echo  $text; ?>
        </div>
    <?php }else{
    jbs_show_success_messages();
    ?>

    <form id="jbs_knobelturnier_form" class="jbs_form" action="" method="POST">
        <fieldset>
            <?php if( get_option('knobelturnier-feld-teamname') == 'on'){ ?>
            <p>
                <label for="teamname"><?php _e('Teamname', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_team" id="teamname" class="required" required type="text" />
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-ansprechpartner') == 'on'){ ?>
            <p>
                <label for="teamname"><?php _e('Ansprechpartner', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_ansprech" id="ansprechpartner" class="required" required type="text" />
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-email') == 'on'){ ?>
            <p>
                <label for="teamname"><?php _e('Email-Adresse', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_mail" id="mail" class="required" required type="email" />
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-1') == 'on'){ ?>
            <p>
                <label for="spieler_1"><?php _e('Spieler 1:', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_sp_1" id="spieler_1" required class="required" type="text"/>
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-2') == 'on'){ ?>
            <p>
                <label for="spieler_2"><?php _e('Spieler 2:', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_sp_2" id="spieler_2" required class="required" type="text"/>
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-3') == 'on'){ ?>
            <p>
                <label for="spieler_3"><?php _e('Spieler 3:', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_sp_3" id="spieler_3" required class="required" type="text"/>
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-4') == 'on'){ ?>
            <p>
                <label for="spieler_4"><?php _e('Spieler 4:', 'jbs-events'); ?>*</label>
                <input name="jbs_kno_sp_4" id="spieler_4" required class="required" type="text"/>
            </p>
            <?php ;} ?>
            <?php if( get_option('knobelturnier-feld-sp-ersatz') == 'on'){ ?>
            <p>
                <label for="spieler_ersatz"><?php _e('Ersatzspieler:', 'jbs-events'); ?></label>
                <input name="jbs_kno_sp_ersatz" id="spieler_ersatz" type="text"/>
            </p>
            <?php ;} ?>
            <p>
                <input type="hidden" name="jbs_kno_nonce" value="<?php echo wp_create_nonce('jbs-kno-nonce'); ?>"/>
                <input type="submit" value="<?php _e('Mannschaft anmelden', 'jbs-events'); ?>"/>
            </p>
        </fieldset>
    </form>
    <?php
    ;}
    return ob_get_clean();}

function jbs_display_edit_pw_form() {
    ob_start();
    global $current_user;
    wp_get_current_user();


    jbs_show_error_messages();
    jbs_show_success_messages();
    ?>

    <form id="jbs_registration_form" class="jbs_form" action="" method="POST">
        <fieldset>
            <p>
                <label for="password"><?php _e('Passwort'); ?></label>
                <input name="jbs_user_pass" id="password" class="required" required type="password"/>
            </p>
            <p>
                <label for="password_again"><?php _e('Passwort wiederholen'); ?></label>
                <input name="jbs_user_pass_confirm" id="password_again" required class="required" type="password"/>
            </p>
            <p>
                <input type="hidden" name="jbs_update_pw_nonce" value="<?php echo wp_create_nonce('jbs-update-pw-nonce'); ?>"/>
                <input type="submit" value="<?php _e('Passwort ändern'); ?>"/>
            </p>
        </fieldset>
    </form>
    <?php
    return ob_get_clean();
}

function jbs_display_profile_form() {
    ob_start();
    global $current_user;
    wp_get_current_user();
    
    jbs_show_success_messages();
    ?>

    <form id="jbs_registration_form" class="jbs_form" action="" method="POST">
        <fieldset>
            <p>
                <label for="jbs_user_Login"><?php _e('Benutzername'); ?></label>
                <input value="<?php echo $current_user->user_login; ?>" name="jbs_user_login" id="jbs_user_login" readonly type="text" />
            </p>
            <p>
                <label for="jbs_user_email"><?php _e('E-Mail'); ?></label>
                <input value="<?php echo $current_user->user_email; ?>" name="jbs_user_email" id="jbs_user_email" class="required" required type="email"/>
            </p>
            <p>
                <label for="jbs_user_first"><?php _e('Vorname'); ?></label>
                <input value="<?php echo esc_attr(get_the_author_meta('first_name', $current_user->ID)); ?>" name="jbs_user_first" id="jbs_user_first" required type="text"/>
            </p>
            <p>
                <label for="jbs_user_last"><?php _e('Nachname'); ?></label>
                <input value="<?php echo esc_attr(get_the_author_meta('last_name', $current_user->ID)); ?>" name="jbs_user_last" id="jbs_user_last" required type="text"/>
            </p>
            <p>
                <label for="jbs_user_street"><?php _e('Straße, Hausnr.'); ?></label>
                <input value="<?php echo esc_attr(get_the_author_meta('strasse', $current_user->ID)); ?>" name="jbs_user_street" id="jbs_user_street" required type="text"/>
                <input value="<?php echo esc_attr(get_the_author_meta('hausnr', $current_user->ID)); ?>" name="jbs_user_street_nr" id="jbs_user_street_nr" required size="3" type="text"/>
            </p>
            <p>
                <label for="jbs_user_plz"><?php _e('PLZ, Ort'); ?></label>
                <input value="<?php echo esc_attr(get_the_author_meta('plz', $current_user->ID)); ?>" name="jbs_user_plz" id="jbs_user_plz" size="8" required type="text"/>
                <input value="<?php echo esc_attr(get_the_author_meta('ort', $current_user->ID)); ?>" name="jbs_user_ort" id="jbs_user_ort" type="text" required />
            </p>
            <p>
                <label for="jbs_user_mobil"><?php _e('Handynummer'); ?></label>
                <input value="<?php echo esc_attr(get_the_author_meta('mobil',$current_user->ID)); ?>" name=jbs_user_mobil id="jbs_user_mobil" required type="text"/>
            </p>
            <p>
                <label for="instrument"><?php _e('Instrument'); ?></label>
                <?php
                if (esc_attr(get_the_author_meta('instrument', $current_user->ID)) == 'Flöte') {
                    $fl = "selected";
                    $schl = "";
                } else if (esc_attr(get_the_author_meta('instrument', $current_user->ID)) == 'Schlagwerk') {
                    $fl = "";
                    $schl = "selected";
                }
                ?>
                <select name="instrument" value="<?php echo esc_attr(get_the_author_meta('instrument', $current_user->ID)); ?>" required>
                    <option value="Flöte" <?php echo $fl; ?> ><?php _e('Flöte','jbs-events'); ?></option>
                    <option value="Schlagwerk"<?php echo $schl; ?> ><?php _e('Schlagwerk','jbs-events'); ?></option>
                </select>
            </p>
            <p>
                <input type="hidden" name="jbs_update_nonce" value="<?php echo wp_create_nonce('jbs-update-nonce'); ?>"/>
                <input type="submit" value="<?php _e('Speichern'); ?>"/>
            </p>
        </fieldset>
    </form>
    <?php
    return ob_get_clean();
}

if (isset($_POST['jbs_kno_nonce']) && wp_verify_nonce($_POST['jbs_kno_nonce'], 'jbs-kno-nonce')){
    
// Set up the mail variables
    global $phpmailer;
        
    if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) {
        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        require_once ABSPATH . WPINC . '/class-smtp.php';
        $phpmailer = new PHPMailer( true );
    }

        
    $teamname = "";
    $spieler1 = "";
    $spieler2 = "";
    $spieler3 = "";
    $spieler4 = "";
    $spielerErsatz = "";
    $mail = "";
    $ansprechpartner = "";
    $file_name = "team.csv";

	if( get_option('knobelturnier-feld-teamname') == 'on' ){$teamname = $_POST["jbs_kno_team"];}
	if( get_option('knobelturnier-feld-sp-1') == 'on' ){$spieler1 = $_POST["jbs_kno_sp_1"];}
	if( get_option('knobelturnier-feld-sp-2') == 'on' ){$spieler2 = $_POST["jbs_kno_sp_2"];}
	if( get_option('knobelturnier-feld-sp-3') == 'on' ){$spieler3 = $_POST["jbs_kno_sp_3"];}
    if( get_option('knobelturnier-feld-sp-4') == 'on' ){$spieler4 = $_POST["jbs_kno_sp_4"];}
    if( get_option('knobelturnier-feld-sp-ersatz') == 'on' ){$spielerErsatz = $_POST["jbs_kno_sp_ersatz"];}
	if( get_option('knobelturnier-feld-sp-ansprechpartner') == 'on' ){$ansprechpartner = $_POST["jbs_kno_ansprech"];}
	if( get_option('knobelturnier-feld-sp-email') == 'on' ){$mail = $_POST["jbs_kno_mail"];}
	
        $time = date("Y-m-d H:i:s");
        insertLogIntoDB('New Team Knobelturnier', $time, 'Teamname:' . $teamname . '|Spieler 1:' . $spieler1 . '|Spieler 2:' . $spieler2 . '|Spieler 3:' . $spieler3 . '|Spieler 4:' . $spieler4 . '|Ersatz:' . $spielerErsatz . '|Ansprechpartner:' . $ansprechpartner . '|E-Mail:' . $mail, '', '', $_SERVER['REMOTE_ADDR']);
        $nachricht = "<html><body>";
        $nachricht .= get_option('knobelturniermessageanmeldung');
        $nachricht .= "</body></html>";
        $variables = 
            array(
                'datum' => get_option('knobelturnier-datum'),
                'ausrichter' => get_option('knobelturnier-ausrichter'),
                'spieler1' => $spieler1,
                'spieler2' => $spieler2,
                'spieler3' => $spieler3,
                'spieler4' => $spieler4,
                'spielerersatz' => $spielerErsatz,
                'team' => $teamname,
                'ansprechpartner' => $ansprechpartner,
                'ansprechemail' => $mail
            );
            foreach($variables as $key => $value){            
                if($key == 'datum'){
                    $format = substr($value, 8,2) . '.';
                    $format = $format . substr($value, 5,2) . '.';
                    $format = $format . substr($value, 0,4);
                    $nachricht = str_replace('{{'.strtoupper($key).'}}', $format, $nachricht);
                }else{             
                    $nachricht = str_replace('{{'.strtoupper($key).'}}', $value, $nachricht);
                }
            }

            add_filter( 'wp_mail_content_type', function( $content_type ) {
                return 'text/html';
            });

            /*
            Text-Datei erstellen anhängen
            */
            //$daten = array($teamname, $spieler_1, $spieler_2, $spieler_3, $spieler_4);
            //$fp = fopen($teamname.'.csv', 'a');
            //fputcsv($fp, $daten);
            //rewind($fp);
            //fclose($fp);
            //$fd = fopen($teamname.'.csv', 'r+');
            //$anhang = chunk_split(base64_encode(fread($fd, filesize($teamname.'.csv'))));

     // Nachrichten senden
     $subj = 'Neue Anmeldung für das interne Knobelturnier' . ' - ' . $teamname;


	 $sent = wp_mail(get_option('knobelturnier-mail-anmeldungen'),$subj,$nachricht);


	 if($sent){
        if(get_option('knobelturnier-mail-anmeldungen-bestaetigung') == 'on'){
            jbs_success()->add('team_recieved', __('Eurer Team wurde angemeldet. Du bekommst zur Bestätigung eine E-Mail von uns.', 'jbs-events'));
        }else{
            jbs_success()->add('team_recieved', __('Eurer Team wurde angemeldet. Es wird keine weitere Bestätigungsmail verschickt.', 'jbs-events'));
        }

        $nachricht = "<html><body>";
        $nachricht .= get_option('knobelturniermessagebestaetigung');
        $nachricht .= "</body></html>";

        foreach($variables as $key => $value){            
            if($key == 'datum'){
               $format = substr($value, 8,2) . '.';
               $format = $format . substr($value, 5,2) . '.';
               $format = $format . substr($value, 0,4);
               $nachricht = str_replace('{{'.strtoupper($key).'}}', $format, $nachricht);
            }else{             
                $nachricht = str_replace('{{'.strtoupper($key).'}}', $value, $nachricht);
            }
        }

        if( get_option('knobelturnier-mail-anmeldungen-bestaetigung') == 'on'){
	       wp_mail($mail,'Danke für eure Anmeldung',$nachricht);
        }

        remove_filter( 'wp_mail_content_type', function( $content_type ) {
                    return 'text/html';
                } );
	 }else{
        jbs_errors()->add('no_team_recieved', __('Es ist ein Fehler aufgetreten. Probiere es nochmal','jbs-events'));
     }   
        // Output the response
        //echo $smtp_debug;

        unset($phpmailer);
        
}

if (isset($_POST["jbs_user_pass"]) && isset($_POST["jbs_user_pass_confirm"]) && wp_verify_nonce($_POST['jbs_update_pw_nonce'], 'jbs-update-pw-nonce')) {

    if($_POST['jbs_user_pass'] != ""){
        $pw = md5($_POST['jbs_user_pass']);
        $pwconfirm = md5($_POST['jbs_user_pass_confirm']);

        if ($pw == $pwconfirm) {
            global $current_user;
            wp_get_current_user();
            global $wpdb;
            $table = $wpdb->prefix . "users";
            $r = $wpdb->update($table, array(
                'user_pass' => $pwconfirm
                    ), array('ID' => $current_user->ID), array('%s'), array('%d'));

            if ($r != false) {
                $time = date("Y-m-d H:i:s");
                insertLogIntoDB('Password edited', $time, 'user-id:' . $current_user->ID . '|pw:' . $pw, $current_user->ID, $current_user->user_login, $_SERVER['REMOTE_ADDR']);
                jbs_success()->add('pw_changed', __('Dein Passwort wurde erfolgreich geändert. Du musst dich neu einloggen! ' )) ;
                echo '<meta http-equiv="refresh" content="3; url='. home_url() .'">';
            }
        }else{
            // Passwörter stimmen nicht überein
            jbs_errors()->add('passwords_dont_match', __('Die Passwörter stimmen nicht überein.'));
        }
    }else{
        // Passwortfeld leer
        jbs_errors()->add('empty_password', __('Kein Passwort eingegeben.'));
    }
}

add_shortcode('jbs_profile', 'jbs_display_profile_form');
add_shortcode('jbs_change_password', 'jbs_display_edit_pw_form');

function insertLogIntoDB($action, $time, $info, $id, $name, $ip) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_log';
    $wpdb->insert($table, array(
        'time' => $time,
        'action' => $action,
        'info' => $info,
        'userid' => $id,
        'user' => $name,
        'ip' => $ip
            ), array(
        '%s', '%s', '%s', '%d', '%s', '%s'
    ));
}



// update records
if (isset($_POST["jbs_user_login"]) && wp_verify_nonce($_POST['jbs_update_nonce'], 'jbs-update-nonce')) {

    $user_login = $_POST['jbs_user_login'];
    $user_email = $_POST["jbs_user_email"];
    $user_first = $_POST["jbs_user_first"];
    $user_last = $_POST["jbs_user_last"];
    $instrument = $_POST["instrument"];
    $mobilnumber = $_POST["jbs_user_mobil"];
    $strasse = $_POST["jbs_user_street"];
    $hausnr = $_POST["jbs_user_street_nr"];
    $plz = $_POST["jbs_user_plz"];
    $ort = $_POST["jbs_user_ort"];

    global $wpdb;
    $table = $wpdb->prefix . "users";
    $r = $wpdb->update($table, array(
        'user_email' => $user_email
            ), array('user_login' => $user_login), array('%s'), array('%s'));

    global $current_user;
    wp_get_current_user();
    //custom meta updaten
    update_user_meta($current_user->ID, 'first_name', $user_first);
    update_user_meta($current_user->ID, 'last_name', $user_last);
    update_user_meta($current_user->ID, 'instrument', $instrument);
    update_user_meta($current_user->ID, 'mobil', $mobilnumber);
    update_user_meta($current_user->ID, 'strasse', $strasse);
    update_user_meta($current_user->ID, 'hausnr', $hausnr);
    update_user_meta($current_user->ID, 'plz', $plz);
    update_user_meta($current_user->ID, 'ort', $ort);

    if ($current_user) {
        // send an email to the admin alerting them of the registration
        //wp_new_user_notification($new_user_id);
        $time = date("Y-m-d H:i:s");
        insertLogIntoDB('Profile edited', $time, 'user-id:' . $current_user->ID . '|first:' . $user_first . '|last:' . $user_last . '|email:' . $user_email . '|instr:' . $instrument . '|mobil:' . $mobilnumber . '|strasse:' . $strasse . '|hausnr:' . $hausnr . '|plz:' . $plz . '|ort:' . $ort . '', $current_user->ID, $user_first . ' ' . $user_last, $_SERVER['REMOTE_ADDR']);

     jbs_success()->add('profile_updated', __('Dein Profil wurde gespeichert.'));

    }
}
?>
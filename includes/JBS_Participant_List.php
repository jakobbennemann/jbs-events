<?php

class JBS_Participant_List extends JBS_List {

    var $found_data = array();
    var $anzahl_pro_seite = 0;
    var $id = -1;
    var $data = array();

    function get_columns() {
        $columns = array(
            'id' => '#',
            'description' => 'Beschreibung',
            'status' => 'Status',
            'note' => 'Notiz'
        );
        return $columns;
    }

    function setEvents($anzahl) {
        $this->anzahl_pro_seite = $anzahl;
    }

    function setUserid($userid) {
        $this->id = $userid;
    }

    function jbs_get_events() {
        global $wpdb;
        $table_participation = $wpdb->prefix . 'jbs_events_participation';
        $table_events = $wpdb->prefix . 'jbs_events';
        $teilnehmer = array();
        $results = $wpdb->get_results("SELECT * FROM $table_participation WHERE userid=$this->id");

        $t = 1;
        foreach ($results as $event) {
            $eventid = $event->eventid;
            $status = $event->status;
            $note = $event->info_participant;
            $name = $wpdb->get_var("SELECT description FROM $table_events WHERE id=$eventid");

            if($status == 0){
                $status = "nimmt teil";
            }else if($status == 1){
                $status = "unsicher";
            }else if($status == 2){
                $status = "nimmt nicht teil";
            }
            $tmp = array('id' => $t, 'description' => $name, 'status' => $status,
                'note' => $note);

            $t++;
            array_push($teilnehmer, $tmp);
        }
        return $teilnehmer;
    }

    function prepare_items() {
        $columns = $this->get_columns();
        $this->data = $this->jbs_get_events();
        $per_page = $this->anzahl_pro_seite;
        $current_page = $this->get_pagenum();
        $total_items = count($this->data);

        $this->found_data = array_slice($this->data, (($current_page - 1) * $per_page), $per_page);

        $this->set_pagination_args(array(
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page                     //WE have to determine how many items to show on a page
        ));
        $this->items = $this->found_data;
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'id':
                return $item[$column_name];
            case 'description':
                return $item[$column_name];
            case 'status':
                return $item[$column_name];
            case 'note':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

}

?>
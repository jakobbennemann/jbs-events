<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php' );
require_once( dirname(__FILE__) . '/JBS_Participant_List.php' );

$userid = $_GET['userid'];
$user = get_userdata($userid);


if (isset($_GET['userid'])) {
    ?>

    <div class = "wrap">
        <h2>Terminübersicht - <strong><?php echo $user->display_name; ?></strong></h2>
        <?php
        $userlist = new JBS_Participant_List();

        //hier werden die Daten aus der DB geladen...
        //danach an die Liste übergeben
        //Tabelle ausgeben
        echo '<div class="wrap">';
        add_thickbox();
        ?>
        <?php
        if (!isset($_POST['s'])) {
            $userlist->setUserid($userid);
            $userlist->setEvents(get_option('events_per_page'));
            $userlist->prepare_items();
            $userlist->display();
        }
        ?>
        <?php echo '</div>'; ?>

    </div>
    <?php
}


?>
<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once (ABSPATH . WPINC . '/pluggable.php');
include_once(dirname(__FILE__) . '/class-jbs-shortcodes.php');



function get_Eventdescription($event_id) {
    global $wpdb;
    $db_table = $wpdb->prefix . 'jbs_events';

    //Abfrage
    $abfrage = $wpdb->get_row("SELECT description FROM " . $db_table . " WHERE id =" . $event_id . "");
    return $abfrage->description;
}

if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    global $current_user;
    wp_get_current_user();
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events';
    $table2 = $wpdb->prefix . 'jbs_events_participation';
    $r = get_Eventdescription($_GET['event']);
    $deleted1 = $wpdb->delete($table2, array('eventid' => $_GET['event']));
    $deleted = $wpdb->delete($table, array('id' => $_GET['event']));
    if ($deleted) {
        $time = date("Y-m-d H:i:s");
        insertLog('Event Deleted', $time, 'id:' . $_GET['event'], $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
        jbs_admin_notice_event_deleted($r);
    }
}

function jbs_organizer_admin_main() {
    if (!current_user_can('edit_pages')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    if (!class_exists('JBS_List')) {
        require_once(dirname(__FILE__) . '/includes/class-jbs-list.php');
    }

    if (isset($_POST['updateevent'])) {

        $eventid = $_POST['event_id'];
        $eventname = $_POST['event_description'];
        $eventdate = $_POST['event_date'];
        $eventtime_start = $_POST['event_time_start'];
        $eventtime_end = $_POST['event_time_end'];
        $eventparticipation = $_POST['event_participation'];
        $eventinfo = $_POST['event_info'];
        $eventparticipant_info = '';
        $link = $_POST['event_link'];
        echo $link;
        $gruppen = $_POST['gruppen'];

        $gr_aktiv = 0;
        $gr_passiv = 0;
        $gr_ehren = 0;
        $gr_sonstige = 0;

        if ($gruppen != "" && $gruppen != NULL) {
            foreach ($gruppen AS $a) {
                if (strpos($a, 'Aktives Mitglied') !== false) {
                    $gr_aktiv = 1;
                }
                if (strpos($a, 'Passives Mitglied') !== false) {
                    $gr_passiv = 1;
                }
                if (strpos($a, 'Ehrenmitglied') !== false) {
                    $gr_ehren = 1;
                }
                if (strpos($a, 'Sonstiges Mitglied') !== false) {
                    $gr_sonstige = 1;
                }
            }
        } else {
            $gr_aktiv = 1;
            $gr_passiv = 1;
            $gr_ehren = 1;
            $gr_sonstige = 1;
        }

        $notification_days = $_POST['event_notification_days'];
        if ($link == "0" OR $link == '' OR $link == 0) {
            $link = 'NULL';
        }else{
            $link = "test";
        }

        if (isset($_POST['event_participant_info'])) {
            $eventparticipant_info = 1;
        } else {
            $eventparticipant_info = 0;
        }

        global $wpdb;
        global $current_user;
        wp_get_current_user();
        $table = $wpdb->prefix . 'jbs_events';
        $updated = $wpdb->update(
                $table, array(
            'date' => $eventdate, // string
            'description' => $eventname, // integer (number) 
            'time_start' => $eventtime_start, // string
            'time_end' => $eventtime_end, // string
            'participation' => $eventparticipation, // string
            'extra_info' => $eventinfo, // string
            'info_participant' => $eventparticipant_info, // string
            'link' => $link,
            'group_aktiv' => $gr_aktiv,
            'group_passiv' => $gr_passiv,
            'group_ehren' => $gr_ehren,
            'group_sonstige' => $gr_sonstige), array('id' => $eventid), array(
            '%s', // value1
            '%s', '%s', // value1
            '%s', '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s' // value2
                ), array('%d')
        );

        if ($updated) {
            $time = date("Y-m-d H:i:s");
            insertLog('Update Event', $time, 'date:' . $eventdate . '|descr:' . $eventname . '|ts:' . $eventtime_start . '|te:' . $eventtime_end . '|partic:' . $eventparticipation . '|info:' . $eventinfo . '|inf.part:' . $eventparticipant_info . '|link:' . $link . '|not.days:' . $notification_days, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
            jbs_admin_notice_event_updated($eventname);
        }
    }
}




/*
 * Inhalt Teilnehmerliste
 */

function jbs_organizer_admin_participants() {

    if (!current_user_can('edit_pages')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    if (!class_exists('JBS_List')) {
        require_once(dirname(__FILE__) . '/includes/class-jbs-list.php');
    }
    ?><?php
    // Liste anlegen
    $termine = new My_Participants_List();

    //hier werden die Daten aus der DB geladen...
    //danach an die Liste übergeben
    //Tabelle ausgeben
    echo '<div class="wrap">';
    add_thickbox();


    echo '<h2>Teilnehmerübersicht</h2>';
    if (!isset($_POST['s'])) {
        $termine->setEvents(get_option('events_per_page'));
        $termine->prepare_items();
        $termine->display();
    } else {
        $termine->setoptions(get_option('events_per_page'));
        $termine->found_data = $termine->find($_POST['s']);
        $termine->prepare_items();
        $termine->display();
    }
    ?>
    <!--    <form method = "post">
            <input type = "hidden" name = "page" value = "termine" />
    <?php $termine->search_box('suchen', 'search_id'); ?>
        </form>-->
    <?php
    echo '</div>';
}


function jbs_admin_notice_updated() {
    ?>
    <div class="updated">
        <p><?php _e('Gespeichert!', 'jbs-organizer'); ?></p>
    </div>
    <?php
}

function jbs_admin_notice_updated2($t) {
    ?>
    <div class="updated">
        <p><?php _e("$t", 'jbs-organizer'); ?></p>
    </div>
    <?php
}

function jbs_admin_notice_event_updated($evt) {
    ?>
    <div class="updated">
        <p><?php _e("Das Event \"$evt\" wurde überarbeitet!", 'jbs-organizer'); ?></p>
    </div>
    <?php
}

function jbs_admin_notice_notificationmails_sent($evt, $count) {
    ?>
    <div class="updated">
        <p><?php _e("Es wurden $count Erinnerungsnachrichten für das Event \"$evt\" verschickt.", 'jbs-organizer'); ?></p>
    </div>
    <?php
}


function jbs_admin_notice_error() {
    ?>
    <div class="error">
        <p><?php _e('Nicht gespeichert!', 'jbs-organizer'); ?></p>
    </div>
    <?php
}







// Ende
?>
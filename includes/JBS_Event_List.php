<?php

class JBS_Event_List extends JBS_List {

    var $found_data = array();
    var $anzahl_pro_seite = 0;
    var $eventid = '';
    var $data = array();

    function get_columns() {
        $columns = array(
            'nr' => '#',
            'name' => 'Name',
            'instrument' => 'Instrument',
            'status' => 'Status',
            'note' => 'Notiz'
        );
        return $columns;
    }

    function setTeilnehmerProSeite($anzahl) {
        $this->anzahl_pro_seite = $anzahl;
    }

    function setEventID($id) {
        $this->eventid = $id;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'Nr' => array('nr', true),
            'Name' => array('name', true),
            'Instrument' => array('instrument', true),
            'Status' => array('status', true),
            'Note' => array('notice', true),
        );
        return $sortable_columns;
    }

    function get_participants($e_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'jbs_events_participation';
        $table_user = $wpdb->prefix . 'users';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $teilnehmer = array();
        $results = $wpdb->get_results("SELECT * FROM $table_name JOIN $table_user ON $table_name.userid=$table_user.ID WHERE $table_name.eventid=$e_id");
        $tmp_nr = 1;
        foreach ($results as $user) {
            $s = "";

            if ($user->status == 0) {
                $s = "nimmt teil";
            } else if ($user->status == 2) {
                $s = "nimmt nicht teil";
            } else if ($user->status == 1) {
                $s = "unsicher";
            }
            
            $instrument = $wpdb->get_row("SELECT meta_value FROM $table_usermeta WHERE user_id=$user->ID AND meta_key='instrument'");
            

            $tmp = array('nr' => $tmp_nr, 'name' => $user->display_name, 'instrument' => $instrument->meta_value,
                'status' => $s, 'note' => $user->info_participant);
            $tmp_nr++;
            array_push($teilnehmer, $tmp);
        }
        return $teilnehmer;
    }

    function prepare_items() {
        $columns = $this->get_columns();
        $this->data = $this->get_participants($this->eventid);
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        usort($this->data, array(&$this, 'usort_reorder'));
        $per_page = $this->anzahl_pro_seite;
        $current_page = $this->get_pagenum();
        $total_items = count($this->data);

        $this->found_data = array_slice($this->data, (($current_page - 1) * $per_page), $per_page);

        $this->set_pagination_args(array(
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page                     //WE have to determine how many items to show on a page
        ));
        $this->items = $this->found_data;
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'nr':
                return $item[$column_name];
            case 'name':
                return $item[$column_name];
            case 'instrument':
                return $item[$column_name];
            case 'status':
                return $item[$column_name];
            case 'note':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

    function usort_reorder($a, $b) {
// Bei keiner Sortierung, dann wieder auf Titel
        $orderby = (!empty($_GET['orderby']) ) ? $_GET['orderby'] : 'name';
// If no order, default to asc
        $order = (!empty($_GET['order']) ) ? $_GET['order'] : 'asc';
// Determine sort order
        $result = strcmp($a[$orderby], $b[$orderby]);
// Send final sort direction to usort
        return ( $order === 'asc' ) ? $result : -$result;
    }

}

?>
<?php
/* Fügt eine Checkbox hinzu */
	add_action( 'add_meta_boxes', 'jbs_meta' );

	function jbs_meta_box() {
	    add_meta_box(
	        'jbs_invisible_section',
	        __('Eingeloggte Benutzer', 'jbs-invisible-sites'),
	        'jbs_render_meta_box_content',
	        'post',
	        'side',
	        'high'
	    );
	    add_meta_box(
	        'jbs_invisible_section',
	        __('Eingeloggte Benutzer', 'jbs-invisible-sites'),
			'jbs_render_meta_box_content',
	        'page',
	        'side',
	        'high'
	    );
	}

	function jbs_meta(){
		if( get_option('intern-guest') == 'on' ){
			jbs_meta_box();
		}
	}


	function jbs_render_meta_box_content( $post )
	{
		wp_nonce_field( plugin_basename( __FILE__ ), 'jbs_invisible_noncename' );

		$microcopy = __('Inhalt für Gäste ausblenden', 'jbs-events');

		$secretthis = get_post_meta($post->ID,'_jbs_invisible_new_field',true);
		if ( $secretthis )
		{
	    echo '<input type="checkbox" class="checkbox" id="jbs_invisible_new_field" name="jbs_invisible_new_field" checked="yes" />';
		  }
		  else
		  {
		    echo '<input type="checkbox" class="checkbox" id="jbs_invisible_new_field" name="jbs_invisible_new_field"  />';
		  }
		  echo '<label for="jbs_invisible_new_field">';
		  echo $microcopy;
		  echo '</label> <br /><br />';
		  $microcopy = __('Inhalt für eingeloggte Benutzer ausblenden', 'jbs-events');

		  $secretthis = get_post_meta($post->ID,'_jbs_invisible_new_field_2',true);
		  if ( $secretthis )
		  {
		    echo '<input type="checkbox" class="checkbox" id="jbs_invisible_new_field_2" name="jbs_invisible_new_field_2" checked="yes" />';
		  }
		  else
		  {
		    echo '<input type="checkbox" class="checkbox" id="jbs_invisible_new_field_2" name="jbs_invisible_new_field_2"  />';
		  }
		  echo '<label for="jbs_invisible_new_field_2">';
		  echo $microcopy;
		  echo '</label>';
	}



	add_action( 'save_post', 'jbs_invisible_save_postdata' );
	function jbs_invisible_save_postdata( $post_id )
	{
	  // awfully lot of security for just saving a checkbox eh?!

	  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	      return;

	  if ( !wp_verify_nonce( $_POST['jbs_invisible_noncename'], plugin_basename( __FILE__ ) ) )
	      return;

	  if ( 'page' == $_POST['post_type'] )
	  {
	    if ( !current_user_can( 'edit_page', $post_id ) )
	        return;
	  }
	  else
	  {
	    if ( !current_user_can( 'edit_post', $post_id ) )
	        return;
	  }

	  $secretthis = $_POST['jbs_invisible_new_field'];
	  $secretthis2 = $_POST['jbs_invisible_new_field_2'];
	  update_post_meta($post_id, '_jbs_invisible_new_field', $secretthis);
	  update_post_meta($post_id, '_jbs_invisible_new_field_2', $secretthis2);
	}


	// limit the visibility of posts/pages if they have the meta value/key pair..
	//add_filter('parse_query', 'jbs_invisible_modified_query');   // both works
	add_filter('pre_get_posts', 'jbs_invisible_modified_query');   // both works

	function jbs_invisible_modified_query( $q )
	{
	  global $wpdb;
	  if ( !is_admin() )
	  {
	    if ( !is_user_logged_in() )
	    {
	      $secrefied = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field' AND meta_value = 'on' ");
	      $q->set('post__not_in', $secrefied );
	    }
	  }
	  return $q;
	}


	// remove from wp_list_pages
	add_filter('wp_list_pages_excludes', 'jbs_invisible_wp_list_pages_exclude_array' );

	function jbs_invisible_wp_list_pages_exclude_array( $exclude_array )
	{
	  global $wpdb;
	  if ( !is_admin() )
	  {
	    if ( !is_user_logged_in() )
	    {
	      $secrefied = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field' AND meta_value = 'on' ");
	      $exclude_array = array_merge( $exclude_array, $secrefied );
	    }else{ // kein admin, eingeloggt
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");
	      $exclude_array = array_merge( $exclude_array, $secrefied2 );
		}
	  }else{ // admin, eingeloggt
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");
	      $exclude_array = array_merge( $exclude_array, $secrefied2 );
	  }
	  return $exclude_array;
	}


	// and remove from wp_nav_menu
	// add_filter('wp_nav_menu_excludes', 'secret_wp_nav_menu_exclude_array'); // this filter dosent work
	add_filter( 'wp_get_nav_menu_items', 'jbs_invisible_wp_nav_menu_exclude_array', null, 3 );

	function jbs_invisible_wp_nav_menu_exclude_array( $items, $menu, $args )
	{
	  global $wpdb;
	  $secrefied = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field' AND meta_value = 'on' ");
	  $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");
	  $secreted_parents = array();
	  $secreted_parents2 = array();

	  if ( !is_admin() )
	  {
	    if ( !is_user_logged_in() )
	    {
	      // Iterate over the items to search and destroy
	      foreach ( $items as $key => $item )
	      {
	        if ( in_array( $item->object_id, $secrefied ) )
	        {
	          $secreted_parents[] = $item->ID;
	          unset( $items[$key] );
	        }

	        // if the parent is secret, then so are the children
	        if ( in_array( $item->menu_item_parent, $secreted_parents ) )
	        {
	          $secreted_parents[] = $item->ID;
	          unset( $items[$key] );
	        }
	      }
	    }else{ // user ist eingeloggt
	      // Iterate over the items to search and destroy
	      foreach ( $items as $key => $item )
	      {
	        if ( in_array( $item->object_id, $secrefied2 ) )
	        {
	          $secreted_parents2[] = $item->ID;
	          unset( $items[$key] );
	        }

	        // if the parent is secret, then so are the children
	        if ( in_array( $item->menu_item_parent, $secreted_parents2 ) )
	        {
	          $secreted_parents2[] = $item->ID;
	          unset( $items[$key] );
	        }
	      }
			
		}
	  }else{ // eingeloggt und admin
	      // Iterate over the items to search and destroy
	      /*oreach ( $items as $key => $item )
	      {
	        if ( in_array( $item->object_id, $secrefied2 ) )
	        {
	          $secreted_parents2[] = $item->ID;
	          unset( $items[$key] );
	        }

	        // if the parent is secret, then so are the children
	        if ( in_array( $item->menu_item_parent, $secreted_parents2 ) )
	        {
	          $secreted_parents2[] = $item->ID;
	          unset( $items[$key] );
	        }
	      }*/
		  	  
	  }
	  return $items;
	}


	// more removal.. // for custom queries, like pages listing subpages.. ( I need this :)
	add_filter('the_posts', 'jbs_invisible_the_posts');

	function jbs_invisible_the_posts( $posts )
	{
	  if ( !is_admin() ) // apparently this does NOT work in ADMIN.. thus the last return $posts are needed.
	  {
	    if ( !is_user_logged_in() )
	    {
	      global $wpdb;
	      $secrefied = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field' AND meta_value = 'on' ");

	      foreach ( $posts as $key => $item )
	      {
	        if ( in_array( $item->ID, $secrefied ) )
	        {
	          unset( $posts[$key] );
	          $posts = array_values($posts);  // rearrange the array
	        }
	      }
	    } else{ // eingeloggt, kein admin
		
	      global $wpdb;
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");

	      foreach ( $posts as $key => $item )
	      {
	        if ( in_array( $item->ID, $secrefied2 ) )
	        {
	          unset( $posts[$key] );
	          $posts = array_values($posts);  // rearrange the array
	        }
	      }
		
		}
	  }else { // eingeloggt und Admin
		  
	      /*global $wpdb;
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");

	      foreach ( $posts as $key => $item )
	      {
	        if ( in_array( $item->ID, $secrefied2 ) )
	        {
	          unset( $posts[$key] );
	          $posts = array_values($posts);  // rearrange the array
	        }
	      }*/
	  }
	  return $posts;
	}
		

	// remove link from next / prev post link navigation... sigh...
	add_filter( 'get_previous_post_where', 'jbs_invisible_navigation_exclude_where' );
	add_filter( 'get_next_post_where', 'jbs_invisible_navigation_exclude_where' );

	function jbs_invisible_navigation_exclude_where( $where )
	{
	  if ( !is_admin() )
	  {
	    if ( !is_user_logged_in() ) // if NOT logged in :)
	    {
	      global $wpdb;
	      $secrefied = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field' AND meta_value = 'on' ");

	      if ( $secrefied )
	      {
	        $secretwhere = '';
	        foreach ( $secrefied as $pid )
	        {
	          $secretwhere .= ' AND p.id not like \''. $pid . '\' ';
	        }
	        $where = $where . $secretwhere;
	      }
	    } else{ // eingeloggt, kein admin
			
	      global $wpdb;
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");

	      if ( $secrefied2 )
	      {
	        $secretwhere2 = '';
	        foreach ( $secrefied2 as $pid )
	        {
	          $secretwhere2 .= ' AND p.id not like \''. $pid . '\' ';
	        }
	        $where = $where . $secretwhere2;
	      }
		}
	  }else { //eingeloggt, admin
		  
	      global $wpdb;
	      $secrefied2 = $wpdb->get_col("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_jbs_invisible_new_field_2' AND meta_value = 'on' ");

	      if ( $secrefied2 )
	      {
	        $secretwhere2 = '';
	        foreach ( $secrefied2 as $pid )
	        {
	          $secretwhere2 .= ' AND p.id not like \''. $pid . '\' ';
	        }
	        $where = $where . $secretwhere2;
	      }
	  }
	  return $where;
	}


?>
<?php
if ( ! class_exists( 'JBS_Table' ) ) {
		require_once( 'class-wp-list-table.php' );
	}

	class Participant_Table extends JBS_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Teilnehmer', 'jbs-events' ), //singular name of the listed records
			'plural'   => __( 'Teilnehmer', 'jbs-events' ), //plural name of the listed records
			'ajax'     => false //should this table support ajax?

		] );

	}

	public function search_box( $text, $input_id ) { ?>
		<p class="search-box">
	    	<label class="screen-reader-text" for="<?php echo $input_id ?>"><?php echo $text; ?>:</label>
	    	<input type="search" id="<?php echo $input_id ?>" name="s" value="<?php _admin_search_query(); ?>" />
	    <?php submit_button( $text, 'button', false, false, array('id' => 'search-submit') ); ?>
	    </p>
	<?php }

	public function filter_box () { ?>
        <span class="filter-box alignleft">
            Einträge filtern:
            <select id="filter_box" name="select_filter">
                <option>-- Alle --</option>
                <option>2016</option>
                <option>2017</option>
            </select>
        </span>
    <?php }

	/** Prepare Items to be displayed */
	public function prepare_items($search = null) {

	  $this->_column_headers = $this->get_column_info();

	  /** Process bulk action */
	  $this->process_bulk_action();

	  $per_page     = $this->get_items_per_page( 'events_per_page', 10 );
	  $current_page = $this->get_pagenum();
	  $total_items  = self::record_count();


	  $this->items = self::getEvents( $per_page, $current_page );

	  $this->set_pagination_args( [
	    'total_items' => $total_items, //WE have to calculate the total number of items
	    'per_page'    => $per_page //WE have to determine how many items to show on a page
	  ] );

	  if($search != null){   
	  	global $wpdb;    
		  // Trim Search Term
	      $search = trim($search);
	       
	      /* Notice how you can search multiple columns for your search term easily, and return one data set */
	      $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$wpdb->prefix."jbs_events WHERE description LIKE '%%%s%%' OR extra_info LIKE '%%%s%%'", $search, $search), ARRAY_A);
	      $this->set_pagination_args( [
	      	'total_items' => count($this->items),
	      	'per_page'	=>	$per_page ] );

	  }
	}

	/** Text displayed when no customer data is available */
	public function no_items() {
	  _e( 'Keine Events verfügbar.', 'jbs-events' );
	}

	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;
		$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}jbs_events";
		return $wpdb->get_var( $sql );
	}

	/** get all Events */
	public static function getEvents( $per_page = 5, $page_number = 1, $year = 'all' ){

		global $wpdb;

	  	$sql = "SELECT * FROM {$wpdb->prefix}jbs_events";

	  	if ( ! empty( $_REQUEST['orderby'] ) ) {
	    	$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
	    	$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
	  	}

	  	$sql .= " LIMIT $per_page";

	  	$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


	  	$result = $wpdb->get_results( $sql, 'ARRAY_A' );

	  	return $result;

	}

	/**
	* Get Coulmns in Table
	*/
	function get_columns() {
			$columns = [
		    	'cb'      => '<input type="checkbox" />',
		  	 	'date'    => __( 'Datum', 'jbs-events' ),
		  		'description' => __( 'Beschreibung', 'jbs-events' ),
		  	    'time_start'    => __( 'Beginn', 'jbs-events' ),
		  		'time_end'    => __( 'Ende', 'jbs-events' ),
		  	    'participation'    => __( 'Teilnahme', 'jbs-events' ),
		  	    'extra_info'    => __( 'Zusatzinfo', 'jbs-events' ),
		  	    'info_participant'    => __( 'Eingabe', 'jbs-events' ),
		  	    'notification'    => __( 'Erinnern', 'jbs-events' )
			];

	  return $columns;
	}

	/** 
	* Sortierbare Spalten erstellen
	*/
	public function get_sortable_columns() {
	  $sortable_columns = array(
	    'date' => array( 'date', true ),
	    'description' => array( 'description', true )
	  );

	  return $sortable_columns;
	}

	/** Bulk Actions */
	public function get_bulk_actions() {
	  $actions = [
	    'bulk-delete' => __('Löschen', 'jbs-events')
	  ];

	  return $actions;
	}

	/**
	* Column Titel/Beschreibung/Notification
	*/
    function column_notification($item) {
        $dir = plugins_url('admin/jbs-events-manipulation.php/', dirname(__FILE__));
        if ($item['participation'] == 1 ) {
            return sprintf('<a href="' . $dir . '?&action=%s&event=%s&width=800&height=560" class="thickbox">Erinnerung verschicken</a>', 'notify', $item['id']);
        }
    }

	/** */
	function column_description( $item ) {

	  // create a nonce
	  $delete_nonce = wp_create_nonce( 'jbs_delete_event' );
	  $edit_nonce = wp_create_nonce( 'jbs_edit_event' );
	  $show_nonce = wp_create_nonce( 'jbs_show_event' );
	  $pdf_nonce = wp_create_nonce( 'jbs_pdf_event' );

	  global $wpdb;
	  $table = $wpdb->prefix . "jbs_events";
	  $result = $wpdb->get_var("SELECT participation FROM $table WHERE id = ".$item['id'].";");

      $dir = plugins_url('admin/jbs-events-manipulation.php', dirname(__FILE__));
      $actions = "";
      if($result == 1){
      $actions = [
            'show' => sprintf('<a href="' . $dir . '?action=%s&event=%s&width=800&height=600&nonce=%s" class="thickbox">'.esc_html__('Anzeigen', 'jbs-events').'</a>', 'show', $item['id'], $show_nonce),
            'pdf' => sprintf('<a href="?page=%s&action=%s&id=%s&nonce=%s">Drucken</a>', $_REQUEST['page'], 'pdf', $item['id'], $pdf_nonce),
            'edit' => sprintf('<a href="'. $dir .'?action=%s&event=%s&width=800&height=600&nonce=%s" class="thickbox">Bearbeiten</a>',   'edit', $item['id'], $edit_nonce),
            'delete' => sprintf('<a href="?page=%s&action=%s&event=%s&nonce=%s">Löschen</a>', $_REQUEST['page'], 'delete', $item['id'], $delete_nonce)
        ];

      }else{
      $actions = [
            'pdf' => sprintf('<a href="?page=%s&action=%s&id=%s&nonce=%s">Drucken</a>', $_REQUEST['page'], 'pdf', $item['id'], $pdf_nonce),
            'edit' => sprintf('<a href="'. $dir .'?action=%s&event=%s&width=800&height=600&nonce=%s" class="thickbox">Bearbeiten</a>',   'edit', $item['id'], $edit_nonce),
            'delete' => sprintf('<a href="?page=%s&action=%s&event=%s&nonce=%s">Löschen</a>', $_REQUEST['page'], 'delete', $item['id'], $delete_nonce)
        ];

      }

        return sprintf('%1$s %2$s', $item['description'], $this->row_actions($actions));
	}


	/** 
	* Default Column
	*/
    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'date':
                return $item[$column_name];
            case 'description':
                return $item[$column_name];
            case 'time_start':
                return $item[$column_name];
            case 'time_end':
                return $item[$column_name];
            case 'participation':
                return $item[$column_name];
            case 'extra_info':
                return $item[$column_name];
            case 'info_participant':
                return $item[$column_name];
            case 'notification':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

    /** 
	* Column Bulk Actions
    */
    function column_cb( $item ) {
	  return sprintf(
	    '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']
	  );
	}

	/** remove Event With id */
	public static function deleteEvent( $id ){
		global $wpdb;
		$ret = $wpdb->delete(
	    	"{$wpdb->prefix}jbs_events",
	    	[ 'id' => $id ],
	    	[ '%d' ]
	  );
		$wpdb->delete(
	    	"{$wpdb->prefix}jbs_events_participation",
	    	[ 'eventid' => $id ],
	    	[ '%d' ]
	  );
		if($ret != false){
			return true;
		}else{
			return false;
		}
	}

	/** get number of Events */
	public static function events_count() {
		global $wpdb;

	  	$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}jbs_events";

	  	return $wpdb->get_var( $sql );
	}

	/** 
	* Process Bulk Actions like show delete
	*/
	public function process_bulk_action() {

	  //Detect when a bulk action is being triggered...
	  if ( 'delete' === $this->current_action() ) {

	    // In our file that handles the request, verify the nonce.
	    $nonce = esc_attr( $_REQUEST['nonce'] );

	    if ( ! wp_verify_nonce( $nonce, 'jbs_delete_event' ) ) {
	      die( 'Go get a life script kiddie' );
	    }
	    else {
	      if(self::deleteEvent( absint( $_GET['event'] ) )){
	      	Helper::settingsUpdated('1 Event wurde erfolgreich gelöscht.');

	      }
	    }

	  }

	  if( 'pdf' === $this->current_action() ){
	    // In our file that handles the request, verify the nonce.
	    $nonce = esc_attr( $_REQUEST['nonce'] );

	    if ( ! wp_verify_nonce( $nonce, 'jbs_pdf_event' ) ) {
	      die( 'Go get a life script kiddie' );
	    }
	    else 
	    {
	        $e_id = $_GET['id'];
	        global $wpdb;
	        $table_name = $wpdb->prefix . "jbs_events_participation";
	        $table_events = $wpdb->prefix . "jbs_events";
	        $eventname = $wpdb->get_var("SELECT description FROM $table_events WHERE id=$e_id;");
	        $eventdate = $wpdb->get_var("SELECT date FROM $table_events WHERE id=$e_id;");

	        //Alle Benutzer holen, die für das Event zugesagt haben.
	        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE eventid=$e_id;");
	        foreach ($result as $row) {
	            $meta = $wpdb->prefix . "usermeta";
	            $instrument = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='instrument' && user_id=$row->userid");
	            $fname = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='first_name' && user_id=$row->userid");
	            $lname = $wpdb->get_var("SELECT meta_value FROM $meta WHERE meta_key='last_name' && user_id=$row->userid");
	            $info = $row->info_participant;
	            $id = $row->userid;

	            if($row->status == 0){
	                $teilnehmerIds[] = array(
	                    'id'  => $id,
	                    'name'  => $lname,
	                    'vname'  => $fname,
	                    'instrument'    =>  $instrument,
	                    'info'  =>  $info);
	            }
	            if($row->status == 1){
	                $unsicherIds[] = array(
	                    'id'  => $id,
	                    'name'  => $lname,
	                    'vname'  => $fname,
	                    'instrument'    =>  $instrument,
	                    'info'  =>  $info);
	            }
	            if($row->status == 2){
	                $absageIds[] = array(
	                    'id'  => $id,
	                    'name'  => $lname,
	                    'vname'  => $fname,
	                    'instrument'    =>  $instrument,
	                    'info'  =>  $info);
	            }
	        }

	        ////////////// HIER DIE PDF

        	$html = '<h4 style="font-weight:normal;padding-bottom:20px;">Zusagen:</h4>';
	        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			$pdf->SetCreator("Jakob Bennemann");
			$pdf->SetAuthor('Spielmannszug Südlohn 1950 e.V.');
			$pdf->SetTitle('Anwesenheitsliste');
			$pdf->SetSubject('');


			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, $eventname . ' | ' . $eventdate);

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('helvetica', '', 9);

			// add a page
			$pdf->AddPage();

			// column titles
			$header = array('', 'Name', 'Vorname', 'Information', 'Instrument');

			// write the html Data

			if(count($teilnehmerIds) > 0){
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->ColoredTable($header, $teilnehmerIds);

			}

			// print colored table


			if(count($absageIds) > 0){
			            $pdf->Ln();
			            $pdf->Ln();

			$html = '<h4 style="font-weight:normal;padding-bottom:20px;">Absagen:</h4>';
			$pdf->writeHTML($html, true, false, true, false, '');

			$pdf->ColoredTable($header, $absageIds);

			}

			if(count($unsicherIds) > 0){
			            $pdf->Ln();
			            $pdf->Ln();

			$html = '<h4 style="font-weight:normal;padding-bottom:20px;">Unsicher:</h4>';
			$pdf->writeHTML($html, true, false, true, false, '');

			$pdf->ColoredTable($header, $unsicherIds);
			}
			// ---------------------------------------------------------

			if( count($teilnehmerIds) <= 0 && count($unsicherIds) <= 0 && count($absageIds) <= 0 ){

			    //keine Infos vorhanden
			    $html = '<h1 style="text-align:center;font-style:italic;font-size:15px;color:#990000;">Es sind keine Informationen vorhanden!</h1>';
			$pdf->writeHTML($html, true, false, true, false, '');
			}

	        ////////////// ENDE DER PDF

			// close and output PDF document
			ob_end_clean();
			ob_start();
			$pdf->Output($eventname . '.pdf', 'D');

	    }
	  	
	  }
	  

	  // If the delete bulk action is triggered
	  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
	       || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
	  ) {

	    $delete_ids = esc_sql( $_POST['bulk-delete'] );

	    // loop over the array of record IDs and delete them
		$count = 0;
	    foreach ( $delete_ids as $id ) {
	      $status = self::deleteEvent( $id );
	      if($status){$count++;}
	    }

	    if($count == count($delete_ids)){
	    	Helper::settingsUpdated($count . " Events wurden erfolgreich gelöscht.");
	    }

	    //wp_redirect( esc_url( add_query_arg() ) );
	    //exit;
	  }
	}
}




	function jbs_admin_notice_notificationmails_sent($evt, $count) {
	    ?>
	    <div class="updated">
	        <p><?php _e("Es wurden $count Erinnerungsnachrichten für das Event \"$evt\" verschickt.", 'jbs-organizer'); ?></p>
	    </div>
	    <?php
	}

	function get_Eventdescription($event_id) {
	    global $wpdb;
	    $db_table = $wpdb->prefix . 'jbs_events';

	    //Abfrage
	    $abfrage = $wpdb->get_row("SELECT description FROM " . $db_table . " WHERE id =" . $event_id . "");
	    return $abfrage->description;
	}


	add_action( 'plugins_loaded', function () {
		JBS_Plugin::get_instance();
	} );


/*
* Entfernt die Sub Menus aus dem Hauptmenu, welche nicht sichtbar sein sollen (alle aus den Einstellungen)
*/
add_action( 'admin_head', 'jbs_remove_menus');
function jbs_remove_menus() {
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-log' );
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-knobelturnier' );
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-shortcodes' );
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-css' );
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-smtp' );
    remove_submenu_page( 'jbs-events', 'jbs-events-settings-intern' );
    //remove_submenu_page( 'jbs-events', 'jbs-events-new' );
}


/*
 * Custom Bulk Action in Users.php
 */
add_filter( 'bulk_actions-users', 'register_my_bulk_actions' );
function register_my_bulk_actions($bulk_actions) {
    $bulk_actions['activate'] = __( 'Aktivieren', 'jbs-events');
    $bulk_actions['deactivate'] = __( 'Deaktivieren', 'jbs-events');
    return $bulk_actions;
}

add_filter( 'handle_bulk_actions-users', 'my_bulk_action_handler', 10, 3 );

function my_bulk_action_handler( $redirect_to, $action_name, $user_ids ) {
    global $current_user;
    wp_get_current_user();
    if ( 'activate' === $action_name ) {
        foreach ( $user_ids as $user_id ) {
            $aktuell = get_user_meta($user_id, 'approval_status', true);
            if($aktuell != 1){
                $time = date("Y-m-d H:i:s");
                $user = get_user_by( 'id', $user_id );
                update_user_meta($user_id, 'approval_status', 1);
                send_activation_mail($user);
                insertLogDB('Approvalstatus changed', $time, 'new Status:' . 1 . '|user-id:' . $user_id, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
            }

        }
        $redirect_to = add_query_arg( 'activate_users_processed', count( $user_ids ), $redirect_to );
        return $redirect_to;
    }

    elseif ( 'deactivate' === $action_name ) {
        foreach ( $user_ids as $user_id ) {
            $aktuell = get_user_meta($user_id, 'approval_status', true);
            if($aktuell != 0) {
                $time = date("Y-m-d H:i:s");
                $user = get_user_by('id', $user_id);
                update_user_meta($user_id, 'approval_status', 0);
                send_deactivation_mail($user);
                insertLogDB('Approvalstatus changed', $time, 'new Status:' . 0 . '|user-id:' . $user_id, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
            }
        }
        $redirect_to = add_query_arg( 'deactivate_users_processed', count( $user_ids ), $redirect_to );
        return $redirect_to;
    }

    else
        return $redirect_to; }

function jbs_assets( $hook ) {
  if( 'jbs-events-settings' == $hook ) {
    //wp_enqueue_style( 'jbs_css', plugin_dir_url( __FILE__ ) . '/css/style.css' );
  }
}
//add_action( 'admin_enqueue_scripts', 'jbs_assets' );


class JBS_Plugin {
	// class instance
	static $instance;
	// customer WP_List_Table object
	public $events_obj;
	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}
	public static function set_screen( $status, $option, $value ) {
		return $value;
	}
	public function plugin_menu() {
		
		$hook = add_menu_page(
			'Termine Übersicht',
			'Termine',
			'edit_pages',
			'jbs-events',
			[ $this, 'plugin_settings_page' ], 'dashicons-smiley',8
		);


  add_submenu_page(
  	'jbs-events', 
  	__('Neues Event','jbs-events'), 
  	__('Neuer Termin', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-new', 
  	[$this, 'jbs_events_new_content']);
	
  add_submenu_page(
  	'jbs-events', 
  	__('Events Einstellungen','jbs-events'), 
  	__('Einstellungen', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings', 
  	[$this, 'jbs_events_settings_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('Intern','jbs-events'), 
  	__('Intern', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-intern', 
  	[$this,'jbs_events_settings_intern_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('Knobelturnier','jbs-events'), 
  	__('Knobelturnier', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-knobelturnier', 
  	[$this,'jbs_events_settings_knobelturnier_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('Style','jbs-events'), 
  	__('Style', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-css', 
  	[$this,'jbs_events_settings_css_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('Shortcodes','jbs-events'), 
  	__('Shortcodes', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-shortcodes', 
  	[$this,'jbs_events_settings_shortcodes_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('SMTP','jbs-events'), 
  	__('SMTP', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-smtp', 
  	[$this,'jbs_events_settings_smtp_content']);

  add_submenu_page(
  	'jbs-events', 
  	__('Log','jbs-events'), 
  	__('Log', 'jbs-events'), 
  	'edit_pages', 
  	'jbs-events-settings-log', 
  	[$this,'jbs_events_settings_log_content']);
		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}

	public function update_event(){

        $eventid = $_POST['event_id'];
        $eventname = $_POST['event_description'];
        $eventdate = $_POST['event_date'];
        $eventtime_start = $_POST['event_time_start'];
        $eventtime_end = $_POST['event_time_end'];
        $eventparticipation = $_POST['event_participation'];
        $eventinfo = $_POST['event_info'];
        $eventparticipant_info = '';
        $link = $_POST['event_link'];
        $gruppen = $_POST['gruppen'];

        $gr_aktiv = 0;
        $gr_passiv = 0;
        $gr_ehren = 0;
        $gr_sonstige = 0;

        if ($gruppen != "" && $gruppen != NULL) {
            foreach ($gruppen AS $a) {
                if (strpos($a, 'Aktives Mitglied') !== false) {
                    $gr_aktiv = 1;
                }
                if (strpos($a, 'Passives Mitglied') !== false) {
                    $gr_passiv = 1;
                }
                if (strpos($a, 'Ehrenmitglied') !== false) {
                    $gr_ehren = 1;
                }
                if (strpos($a, 'Sonstiges Mitglied') !== false) {
                    $gr_sonstige = 1;
                }
            }
        } else {
            $gr_aktiv = 1;
            $gr_passiv = 1;
            $gr_ehren = 1;
            $gr_sonstige = 1;
        }

        if($link == "0"){$link = NULL;}

        if (isset($_POST['event_participant_info'])) {
            $eventparticipant_info = 1;
        } else {
            $eventparticipant_info = 0;
        }


        global $wpdb;
        global $current_user;
        wp_get_current_user();
        $table = $wpdb->prefix . 'jbs_events';
        $updated = $wpdb->update(
                $table, array(
            'date' => $eventdate, // string
            'description' => $eventname, // integer (number) 
            'time_start' => $eventtime_start, // string
            'time_end' => $eventtime_end, // string
            'participation' => $eventparticipation, // string
            'extra_info' => $eventinfo, // string
            'info_participant' => $eventparticipant_info, // string
            'link' => $link,
            'group_aktiv' => $gr_aktiv,
            'group_passiv' => $gr_passiv,
            'group_ehren' => $gr_ehren,
            'group_sonstige' => $gr_sonstige), array('id' => $eventid), array(
            '%s', // value1
            '%s', '%s', // value1
            '%s', '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s' // value2
                ), array('%d')
        );

        if ($updated) {
            $time = date("Y-m-d H:i:s");
            Helper::newLog('Update Event', $time, 'date:' . $eventdate . '|descr:' . $eventname . '|ts:' . $eventtime_start . '|te:' . $eventtime_end . '|partic:' . $eventparticipation . '|info:' . $eventinfo . '|inf.part:' . $eventparticipant_info . '|link:' . $link, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
            Helper::settingsUpdated('Der Termin ' . $eventname . ' wurde bearbeitet.' );
        }
    }

	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		?>
		<div class="wrap">
			<?php
       
			//Notificationmails schicken
			if( isset( $_POST['notificationmail']) ){ 
				$gruppen = $_POST['gruppen'];
		        $nachricht = $_POST['message'];
		        $eventid = $_POST['event_id'];
		        $st = $_POST['status'];
		        $groups = $_POST['groups'];
		        $unsicher = "";
		        $keineAngabe = "";
		        foreach ($st as $a) {
		            if ($a == 1) {
		                $unsicher = 1;
		            }
		            if ($a == 3) {
		                $keineAngabe = 3;
		            }
		        }
		        if ($gruppen == null || $gruppen == "") {
		            $gruppen = $groups;
		        }

		        // alle Mitglieder mit dem übergebenen Status finden 0=dabei 1=unsicher 2=absage 3=keine Angabe
		        global $wpdb;
		        $table_events = $wpdb->prefix . 'jbs_events_participation';
		        $table_usermeta = $wpdb->prefix . 'usermeta';

		        $liste_userids_mail = array();
		        $member_ids = array();

		        if ($unsicher == 1) {
		            $results = $wpdb->get_results("SELECT * FROM $table_events WHERE status = 1 AND eventid = $eventid");

		            foreach ($results AS $result) {
		                array_push($member_ids, $result->userid);
		            }

		            //nachdem alle unsicheren userids vorhanden sind, vergleiche sie mit gruppenzugehörigkeit

		            foreach ($gruppen AS $gruppe) {
		                $results = $wpdb->get_results("SELECT * FROM $table_usermeta WHERE meta_key = 'gruppe' AND meta_value = '$gruppe'");

		                foreach ($results as $r) {
		                    for ($i = 0; $i < count($member_ids); $i++) {
		                        if ($r->user_id == $member_ids[$i]) {
		                            array_push($liste_userids_mail, $r->user_id);
		                        }
		                    }
		                }
		            }
		        }

		        if ($keineAngabe == 3) {
		            // alle Mitglieder raussuchen, die noch keine Angabe gemacht haben
		            foreach ($gruppen as $gruppe) {
		                $results = $wpdb->get_results("SELECT $table_usermeta.user_id FROM $table_usermeta WHERE $table_usermeta.user_id NOT IN (SELECT userid FROM $table_events WHERE $table_events.eventid = $eventid) AND $table_usermeta.meta_key = 'gruppe' AND $table_usermeta.meta_value = '" . $gruppe . "'");

		                foreach ($results as $n) {
		                    array_push($liste_userids_mail, $n->user_id);
		                }
		            }
		        }

		        // 
		        $mailadressen = array();
		        $table_user = $wpdb->prefix . 'users';
		        $results = $wpdb->get_results("SELECT ID,user_email FROM $table_user");
		        foreach ($results as $result) {
		            for ($uid = 0; $uid < count($liste_userids_mail); $uid++) {
		                if ($liste_userids_mail[$uid] == $result->ID) {
		                    array_push($mailadressen, $result->user_email);
		                }
		            }
		        }

		        // Nachrichten senden
	            add_filter( 'wp_mail_content_type', function( $content_type ) {
	                return 'text/html';
	            });
		        $subj = 'Erinnerung für "' . get_Eventdescription($eventid) . '"';
        		$nachricht = "<html><body>";
		        $nachricht .= get_option('erinnerungsmessagetext');
        		$nachricht .= "</body></html>";

        		// PLatzhalter ersetzen
        		$variables = 
	            array(
	                'termin' => get_Eventdescription($eventid)
	            );
		        foreach($variables as $key => $value){            
		            if($key == 'datum'){
		               $format = substr($value, 8,2) . '.';
		               $format = $format . substr($value, 5,2) . '.';
		               $format = $format . substr($value, 0,4);
		               $nachricht = str_replace('{{'.strtoupper($key).'}}', $format, $nachricht);
		            }else{             
		                $nachricht = str_replace('{{'.strtoupper($key).'}}', $value, $nachricht);
		            }
		        }

		        // Platzhalter Ende
				$headers[] = 'From: '.get_option('smtp-name').' <'.get_option('smtp-mail').'>';
		        $sent = wp_mail($mailadressen, $subj, $nachricht,$headers);
		        if ($sent) {
		            jbs_admin_notice_notificationmails_sent(get_Eventdescription($eventid), count($mailadressen));
		        } else {
		            jbs_admin_notice_notificationmails_sent(get_Eventdescription($eventid), 0);
		        }
        		remove_filter( 'wp_mail_content_type', function( $content_type ) {
                    return 'text/html';
                } );

			} ?>
			<h2><?php add_thickbox(); esc_html_e('Terminübersicht','jbs-events'); ?>
			<a href="admin.php?page=jbs-events-new" class="page-title-action">Neuer Termin</a>
			<?php if( isset($_POST['updateevent']) && wp_verify_nonce($_REQUEST['_wpnonce'], 'jbs-update-event')  ){
				$this->update_event( $_POST['event_id'] );
			}

			?>
			<?php if( isset($_POST['s']) ){ ?><span class="subtitle">Suchergebnisse für: <?php echo '"'.$_POST['s'].'"'; ?></span><?php ;} ?>
            </h2>

			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-2">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<input type="hidden" value="<?php echo $_REQUEST['page'] ?>" />
								<?php
                                //$this->events_obj->filter_box();
								$this->events_obj->search_box('Suchen', 'jbs-events');
								if( isset($_POST['s']) ){
						            $this->events_obj->prepare_items($_POST['s']);
						        } else {
								$this->events_obj->prepare_items();
						        }
								$this->events_obj->display(); ?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	function jbs_events_settings_smtp_content(){

	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}
		// Load the options
	global $wpms_options, $phpmailer;
	
	// Make sure the PHPMailer class has been instantiated 
	// (copied verbatim from wp-includes/pluggable.php)
	// (Re)create it, if it's gone missing
	if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) {
		require_once ABSPATH . WPINC . '/class-phpmailer.php';
		require_once ABSPATH . WPINC . '/class-smtp.php';
		$phpmailer = new PHPMailer( true );
	}

	// Send a test mail if necessary
	if (isset($_POST['wpms_action']) && $_POST['wpms_action'] == __('Send Test', 'jbs-events') && is_email($_POST['to'])) {
		
		check_admin_referer('test-email');
		
		// Set up the mail variables
		$to = $_POST['to'];
		$subject = 'JBS Events: ' . __('Testmail an ', 'jbs-events') . $to;
		$message = __('Dies ist eine Testmail, die von dem Plugin JBS Events verschickt wurde!', 'jbs-events');
		
		// Set SMTPDebug to true
		$phpmailer->SMTPDebug = true;
		
		// Start output buffering to grab smtp debugging output
		ob_start();

		// Send the test mail
		$result = wp_mail($to,$subject,$message);
		
		// Strip out the language strings which confuse users
		//unset($phpmailer->language);
		// This property became protected in WP 3.2
		
		// Grab the smtp debugging output
		$smtp_debug = ob_get_clean();
		
		// Output the response
		?>

		<div id="message" class="updated fade"><p><strong><?php _e('Testnachricht verschickt', 'jbs-events'); ?></strong></p>
		<p><?php _e('Der Ergebnis ist:', 'jbs-events'); ?></p>
		<pre><?php var_dump($result); ?></pre>
		<p><?php _e('The full debugging output is shown below:', 'jbs-events'); ?></p>
		<pre><?php var_dump($phpmailer); ?></pre>
		<p><?php _e('The SMTP debugging output is shown below:', 'jbs-events'); ?></p>
		<pre><?php echo $smtp_debug ?></pre>
		</div>
				<?php
				
				// Destroy $phpmailer so it doesn't cause issues later
				unset($phpmailer);

	} ?>
		<div class="wrap">

		    <h2><?php esc_html_e('Einstellungen'); ?></h2>

		    <h2 class="nav-tab-wrapper">
		      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
		      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
		      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
			      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
		      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
		      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
		      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
		    </h2>

			<form method="post" action="options.php">
		    	<?php settings_fields('jbs-option-group'); ?>
		    	<?php do_settings_sections('jbs-option-group'); ?>

				<table class="optiontable form-table">
					<tr valign="top">
						<th scope="row"><label for="mail_from"><?php _e('Absender E-Mail', 'jbs-events'); ?></label></th>
						<td>
							<input name="mail_from" type="text" id="mail_from" value="<?php print(get_option('mail_from')); ?>" size="40" class="regular-text" />
							<span class="description"><?php _e('You can specify the email address that emails should be sent from. If you leave this blank, the default email will be used.', 'jbs-events'); if(get_option('db_version') < 6124) { print('<br /><span style="color: red;">'); _e('<strong>Please Note:</strong> You appear to be using a version of WordPress prior to 2.3. Please ignore the From Name field and instead enter Name&lt;email@domain.com&gt; in this field.', 'jbs-events'); print('</span>'); } ?></span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="mail_from_name"><?php _e('Absender Name', 'jbs-events'); ?></label></th>
						<td>
							<input name="mail_from_name" type="text" id="mail_from_name" value="<?php print(get_option('mail_from_name')); ?>" size="40" class="regular-text" />
							<span class="description"><?php _e('You can specify the name that emails should be sent from. If you leave this blank, the emails will be sent from WordPress.', 'jbs-events'); ?></span>
						</td>
					</tr>
				</table>

				<table class="optiontable form-table">
					<tr valign="top">
						<th scope="row"><?php _e('Mailer', 'jbs-events'); ?> </th>
						<td>
							<fieldset>
								<legend class="screen-reader-text">
									<span><?php _e('Mailer', 'jbs-events'); ?></span>
								</legend>
								<p>
									<input id="mailer_smtp" type="radio" name="mailer" value="smtp" <?php checked('smtp', get_option('mailer')); ?> />
									<label for="mailer_smtp"><?php _e('Send all WordPress emails via SMTP.', 'jbs-events'); ?></label>
								</p>
								<p>
									<input id="mailer_mail" type="radio" name="mailer" value="mail" <?php checked('mail', get_option('mailer')); ?> />
									<label for="mailer_mail"><?php _e('Use the PHP mail() function to send emails.', 'jbs-events'); ?></label>
								</p>
							</fieldset>
						</td>
					</tr>
				</table>


				<table class="optiontable form-table">
					<tr valign="top">
						<th scope="row"><?php _e('Return Path', 'jbs-events'); ?> </th>
						<td>
							<fieldset>
								<legend class="screen-reader-text">
									<span><?php _e('Return Path', 'jbs-events'); ?></span>
								</legend>
								<label for="mail_set_return_path">
									<input name="mail_set_return_path" type="checkbox" id="mail_set_return_path" value="true" <?php checked('true', get_option('mail_set_return_path')); ?> />
									<?php _e('Set the return-path to match the From Email'); ?>
								</label>
							</fieldset>
						</td>
					</tr>
				</table>

				<h3><?php _e('SMTP Options', 'jbs-events'); ?></h3>
				<p><?php _e('These options only apply if you have chosen to send mail by SMTP above.', 'jbs-events'); ?></p>

				<table class="optiontable form-table">
				<tr valign="top">
				<th scope="row"><label for="smtp_host"><?php _e('SMTP Host', 'jbs-events'); ?></label></th>
				<td><input name="smtp_host" type="text" id="smtp_host" value="<?php print(get_option('smtp_host')); ?>" size="40" class="regular-text" /></td>
				</tr>
				<tr valign="top">
				<th scope="row"><label for="smtp_port"><?php _e('SMTP Port', 'jbs-events'); ?></label></th>
				<td><input name="smtp_port" type="text" id="smtp_port" value="<?php print(get_option('smtp_port')); ?>" size="6" class="regular-text" /></td>
				</tr>
				<tr valign="top">
				<th scope="row"><?php _e('Encryption', 'jbs-events'); ?> </th>
				<td><fieldset><legend class="screen-reader-text"><span><?php _e('Verschlüsselung', 'jbs-events'); ?></span></legend>
				<input id="smtp_ssl_none" type="radio" name="smtp_ssl" value="none" <?php checked('none', get_option('smtp_ssl')); ?> />
				<label for="smtp_ssl_none"><span><?php _e('No encryption.', 'jbs-events'); ?></span></label><br />
				<input id="smtp_ssl_ssl" type="radio" name="smtp_ssl" value="ssl" <?php checked('ssl', get_option('smtp_ssl')); ?> />
				<label for="smtp_ssl_ssl"><span><?php _e('Use SSL encryption.', 'jbs-events'); ?></span></label><br />
				<input id="smtp_ssl_tls" type="radio" name="smtp_ssl" value="tls" <?php checked('tls', get_option('smtp_ssl')); ?> />
				<label for="smtp_ssl_tls"><span><?php _e('Use TLS encryption. This is not the same as STARTTLS. For most servers SSL is the recommended option.', 'jbs-events'); ?></span></label>
				</td>
				</tr>
				<tr valign="top">
				<th scope="row"><?php _e('Authentication', 'jbs-events'); ?> </th>
				<td>
				<input id="smtp_auth_false" type="radio" name="smtp_auth" value="false" <?php checked('false', get_option('smtp_auth')); ?> />
				<label for="smtp_auth_false"><span><?php _e('No: Do not use SMTP authentication.', 'jbs-events'); ?></span></label><br />
				<input id="smtp_auth_true" type="radio" name="smtp_auth" value="true" <?php checked('true', get_option('smtp_auth')); ?> />
				<label for="smtp_auth_true"><span><?php _e('Yes: Use SMTP authentication.', 'jbs-events'); ?></span></label><br />
				<span class="description"><?php _e('If this is set to no, the values below are ignored.', 'jbs-events'); ?></span>
				</td>
				</tr>
				<tr valign="top">
				<th scope="row"><label for="smtp_user"><?php _e('Username', 'jbs-events'); ?></label></th>
				<td><input name="smtp_user" type="text" id="smtp_user" value="<?php print(get_option('smtp_user')); ?>" size="40" class="code" /></td>
				</tr>
				<tr valign="top">
				<th scope="row"><label for="smtp_pass"><?php _e('Passwort', 'jbs-events'); ?></label></th>
				<td><input name="smtp_pass" type="password" id="smtp_pass" value="<?php print(get_option('smtp_pass')); ?>" size="40" class="code" /></td>
				</tr>
				</table>
				<?php submit_button(); ?>
			</form>

			<h3><?php _e('Send a Test Email', 'jbs-events'); ?></h3>

			<form method="POST" action="<?php echo admin_url(); ?>admin.php?page=jbs-events-settings-smtp">
			<?php wp_nonce_field('test-email'); ?>
			<table class="optiontable form-table">
			<tr valign="top">
			<th scope="row"><label for="to"><?php _e('An:', 'jbs-events'); ?></label></th>
			<td><input name="to" type="text" id="to" value="" size="40" class="code" />
			<span class="description"><?php _e('Type an email address here and then click Send Test to generate a test email.', 'jbs-events'); ?></span></td>
			</tr>
			</table>
			<p class="submit"><input type="submit" name="wpms_action" id="wpms_action" class="button-primary" value="<?php _e('Send Test', 'jbs-events'); ?>" /></p>
			</form>
		</div>
		<?php
	}


/*
* Ansicht der Einstellungen - Tab 1
*/
function jbs_events_settings_content() {
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}

  ?>
  <div class="wrap">
    <h2><?php esc_html_e('Einstellungen'); ?></h2>

    <h2 class="nav-tab-wrapper">
      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
    </h2>

    <form method="post" action="options.php">
    <?php settings_fields('jbs-option-general-group'); ?>
    <?php do_settings_sections('jbs-general-option-group'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Standardgruppe für neue Benutzer</th>
                <td>
                	<select name="standard-gruppe" required >
                	    <option value="Aktives Mitglied" <?php echo ((esc_attr(get_option('standard-gruppe'))) == 'Aktives Mitglied') ? 'selected' : ""; ?>>Aktives Mitglied</option>
                        <option value="Passives Mitglied" <?php echo ((esc_attr(get_option('standard-gruppe'))) == 'Passives Mitglied') ? 'selected' : ""; ?>>Passives Mitglied</option>
                        <option value="Ehrenmitglied" <?php echo ((esc_attr(get_option('standard-gruppe'))) == 'Ehrenmitglied') ? 'selected' : ""; ?>>Ehrenmitglied</option>
                        <option value="Sonstiges Mitglied" <?php echo ((esc_attr(get_option('standard-gruppe'))) == 'Sonstiges Mitglied') ? 'selected' : ""; ?>>Sonstiges Mitglied</option>
                    </select><br />
                    <span class="description"><?php _e("Jedes neue Mitglied bekommt diese Gruppe als Standard", 'jbs-events'); ?></span>
                </td>
            </tr>
            <tr valign="top">
            	<th scope="row">Manuelle Aktivierung</th>
            	<td>
            		<input type="checkbox" name="standard-aktivierung" <?php echo ((esc_attr(get_option('standard-aktivierung'))) == 'on') ? 'checked' : ""; ?> />
            		<span class="description">Wenn aktiviert, müssen neue Benutzer erst durch einen Admin freigeschaltet werden.</span>
            	</td>
            </tr>
            <tr valign="top">
            	<th scope="row">Benachrichtigung</th>
            	<td>
            		<input type="checkbox" name="register-new-admin" <?php echo ((esc_attr(get_option('register-new-admin'))) == 'on') ? 'checked' : ""; ?> /> Adminnachricht<br />
            		<span class="description">Eine E-Mail an den Admin, bei neuer Registrierung schicken.</span><br /><br />
            		<input type="checkbox" name="register-new-user" <?php echo ((esc_attr(get_option('register-new-user'))) == 'on') ? 'checked' : ""; ?> /> Benutzernachricht<br />
            		<span class="description">Eine Anmeldebestätigung an den neuen Benutzer schicken.</span>
            	</td>
            </tr>
            <tr valign="top">
            <th scope="row">Registrierungsfelder</th>
            <td>
            	<input type="checkbox" name="register-username" <?php echo ((esc_attr(get_option('register-username'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Benutzername','jbs-events'); ?><br />
            	<input type="checkbox" name="register-email" <?php echo ((esc_attr(get_option('register-email'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('E-Mail Adresse','jbs-events'); ?><br />
            	<input type="checkbox" name="register-password" <?php echo ((esc_attr(get_option('register-password'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Passwort','jbs-events'); ?><br />
            	<input type="checkbox" name="register-password2" <?php echo ((esc_attr(get_option('register-password2'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Passwort bestätigen','jbs-events'); ?><br />
            	<input type="checkbox" name="register-first-name" <?php echo ((esc_attr(get_option('register-first-name'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Vorname','jbs-events'); ?><br />
            	<input type="checkbox" name="register-last-name" <?php echo ((esc_attr(get_option('register-last-name'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Nachname','jbs-events'); ?><br />
            	<input type="checkbox" name="register-street" <?php echo ((esc_attr(get_option('register-street'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Straße','jbs-events'); ?><br />
            	<input type="checkbox" name="register-nr" <?php echo ((esc_attr(get_option('register-nr'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Hausnummer','jbs-events'); ?><br />
            	<input type="checkbox" name="register-plz" <?php echo ((esc_attr(get_option('register-plz'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('PLZ','jbs-events'); ?><br />
            	<input type="checkbox" name="register-ort" <?php echo ((esc_attr(get_option('register-ort'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Ort','jbs-events'); ?><br />
            	<input type="checkbox" name="register-mobil" <?php echo ((esc_attr(get_option('register-mobil'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Handynummer','jbs-events'); ?><br />
            	<input type="checkbox" name="register-instrument" <?php echo ((esc_attr(get_option('register-instrument'))) == 'on') ? 'checked' : ""; ?> /><?php esc_html_e('Instrument','jbs-events'); ?><br />
            	<span class="description"><?php esc_html_e('Welche Felder sollen im Registrierungsformular angezeigt werden?','jbs-events'); ?></span>
            </td>
            </tr>
            <tr valign="top">
            	<th scope="row">Blacklist-Domains</th>
            	<td>
            		<textarea style="padding:0;" rows="5" cols="30" name="register-blacklist"><?php echo get_option('register-blacklist'); ?></textarea><br />
            		<span class="description">Eingetragene Domains werden bei der Registrierung blockiert.<br />Mehrere Werte mit einem Komma trennen.</span>
            	</td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
  </div>
  <?php
}

function jbs_events_settings_intern_content(){
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}
	?>
	<div class="wrap">
	    <h2><?php esc_html_e('Einstellungen'); ?></h2>

	    <h2 class="nav-tab-wrapper">
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
	    </h2>

	    <form method="post" action="options.php">
	    <?php settings_fields('jbs-option-intern-group'); ?>
	    <?php do_settings_sections('jbs-general-intern-group'); ?>
	        <table class="form-table">
	            <tr valign="top">
	                <th scope="row">Seiten privatisieren</th>
	                <td>
	                	<input type="checkbox" name="intern-guest" value="on" <?php echo ((esc_attr(get_option('intern-guest'))) == 'on') ? 'checked' : ""; ?> />Seiten für Gäste sperren können<br /><span class="description">
	                		<?php _e("Fügt eine Meta Box für Seiten und Beiträge hinzu.", 'jbs-events'); ?>
	                	</span>
	            	</td>
	            </tr>
	            <tr valign="top">
	                <th scope="row">Weiterleitung Gäste</th>
	                <td>
	                	<?php $args = array('selected' => get_option('page-guests-redirect'), 'name'	=>	'page-guests-redirect');
					wp_dropdown_pages($args); ?><br />
	                    <span class="description"><?php _e("Nicht-eingeloggte Gäste werden auf diese Seite weitergeleitet, wenn sie eine geschützte Seite aufrufen wollen", 'jbs-events'); ?></span>
	                </td>
	            </tr>
	            <tr valign="top">
	                <th scope="row">Interne Seite</th>
	                <td>
	                	<?php $args = array('selected' => get_option('page-users-redirect'), 'name'	=>	'page-users-redirect');
					wp_dropdown_pages($args); ?><br />
	                    <span class="description"><?php _e("Nach einem Login werden Benutzer auf diese Seite weitergeleitet", 'jbs-events'); ?></span>
	                </td>
	            </tr>
	            <tr valign="top">
	                <th scope="row">Erinnerungsmail</th>
	            	<td>
					    <?php
					    $content = get_option('erinnerungsmessagetext') ;
					    $editor_id = "erinnerungsmessagetext";
						wp_editor( html_entity_decode($content), $editor_id );
						?>
					    <span class="description"><?php _e('Inhalt der Erinnerungsmail für einen Termin', 'jbs-events'); ?><br />
					    <?php _e('<b>{{TERMIN}}</b> kann verwendet werden.','jbs-events'); ?></span>
	            	</td>
	            </tr>
	        </table>
	        <?php submit_button(); ?>
	    </form>
    </div>
	<?php
}


/*
* Inhalt der Seite, um einen neuen Termin hinzuzufügen.
* Hier kann ein neuer Termin angelegt werden.
*/
function jbs_events_new_content(){
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}
	?>
	<div class="wrap">

		<?php if( isset($_POST['newevent']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'jbs-new-event' ) ){
			// Termin zur DB hinzufügen

		    global $current_user;
		    wp_get_current_user();
		    $eventname = $_POST['event_description'];
		    $eventdate = $_POST['event_date'];
		    $eventtime_start = $_POST['event_time_start'];
		    $eventtime_end = $_POST['event_time_end'];
		    $eventparticipation = $_POST['event_participation'];
		    $eventinfo = $_POST['event_info'];
		    $eventparticipant_info = '';
		    $link = $_POST['event_link'];
			if($link == 0){
				$link = NULL;
			}
		    $notification_days = $_POST['event_notification_days'];
		    $gruppen = $_POST['gruppen'];
		    $gr_aktiv = 0;
		    $gr_passiv = 0;
		    $gr_ehren = 0;
		    $gr_sonstige = 0;

		    if ($gruppen != "" && $gruppen != NULL) {
		        foreach ($gruppen AS $a) {
		            if (strpos($a, 'Aktives Mitglied') !== false) {
		                $gr_aktiv = 1;
		            }
		            if (strpos($a, 'Passives Mitglied') !== false) {
		                $gr_passiv = 1;
		            }
		            if (strpos($a, 'Ehrenmitglied') !== false) {
		                $gr_ehren = 1;
		            }
		            if (strpos($a, 'Sonstiges Mitglied') !== false) {
		                $gr_sonstige = 1;
		            }
		        }
		    } else {
		        $gr_aktiv = 1;
		        $gr_passiv = 1;
		        $gr_ehren = 1;
		        $gr_sonstige = 1;
		    }

		    if (isset($_POST['event_participant_info'])) {
		        $eventparticipant_info = 1;
		    } else {
		        $eventparticipant_info = 0;
		    }

		    if($link == "0"){$link = NULL;}

		    global $wpdb;
		    $table = $wpdb->prefix . 'jbs_events';


		    $wpdb->insert($table, array(
		        'date' => $eventdate,
		        'description' => $eventname,
		        'time_start' => $eventtime_start,
		        'time_end' => $eventtime_end,
		        'participation' => $eventparticipation,
		        'extra_info' => $eventinfo,
		        'link' => $link,
		        'notification' => $notification_days,
		        'info_participant' => $eventparticipant_info,
		        'group_aktiv' => $gr_aktiv,
		        'group_passiv' => $gr_passiv,
		        'group_ehren' => $gr_ehren,
		        'group_sonstige' => $gr_sonstige), array(
		        '%s',
		        '%s',
		        '%s',
		        '%s',
		        '%d',
		        '%s',
		        '%s',
		        '%d',
		        '%d',
		        '%d', '%d', '%d', '%d'));
		    if ($wpdb->insert_id != null) {
		        $time = date("Y-m-d H:i:s");
		        Helper::newLog('New Event', $time, 'date:' . $eventdate . '|descr:' . $eventname . '|ts:' . $eventtime_start . '|te:' . $eventtime_end . '|partic:' . $eventparticipation . '|info:' . $eventinfo . '|inf.part:' . $eventparticipant_info . '|link:' . $link . '|not.days:' . $notification_days, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);

				Helper::settingsUpdated("Der Termin wurde erfolgreich angelegt.");
		    } else {
		    	Helper::updateFailed("Es ist ein Fehler aufgetreten.");
		    }
		} ?>
		<h2><?php esc_html_e('Neuer Termin hinzufügen'); ?>
		<a href="admin.php?page=jbs-events" class="page-title-action">zurück</a></h2>

        <form method="post" action="?page=jbs-events-new">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Bezeichnung: <span style='color:#d54e21;'>*</span></th>
                    <td><input type="text" name="event_description" value="" required /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Datum: <span style='color:#d54e21;'>*</span></th>
                    <td><input type="date" name="event_date" value="" required /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Uhrzeit (Beginn):</th>
                    <td><input type="time" name="event_time_start" value="" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Uhrzeit (Ende):</th>
                    <td><input type="time" name="event_time_end" value="" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Mitgliederteilnahme: <span style='color:#d54e21;'>*</span></th>
                    <td><select name="event_participation" value="" required>
                            <option value="1" selected>mit Teilnahmemöglichkeit</option>
                            <option value="0">als Information</option>
                        </select><br />
                    	<span class="description">Jeder Benutzer, der unten ausgewählten Benutzergruppen bekommt die Möglichkeit an diesem Termin teilzuhmen.</span></td>
                </tr>
                <tr>
                    <th scope="row">Eventinformation:</th>
                    <td><textarea rows="6" cols="40" name="event_info"></textarea><br />
                    	<span class="description">evtl. kurze Info, wie Abfahrtsort.</span></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Beitrag verlinken:</th>
                    <td><select name="event_link" value="">

                            <option value="0" selected>-- keine Verlinkung --</option>
                            <?php
                            global $post;
                            $args = array('order' => 'ASC', 'orderby' => 'title');
                            $lastposts = get_posts($args);
                            foreach ($lastposts as $post) :
                                setup_postdata($post);
                                ?>
                                <option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>

                                <?php
                            endforeach;
                            wp_reset_postdata();
                            ?>

                        </select><br />
                    	<span class="description">Zugehörigen Beitrag wählen.</span></td>
                </tr>
                <tr>
                    <th scope="row">Eingabefeld für Teilnehmer anzeigen:</th>
                    <td><input type="checkbox" name="event_participant_info"><br />
                    	<span class="description">Den Benutzern eine Möglichkeit geben, selbst eine Info zu diesem Termin abgeben (z.B. Grund der Absage).</span></td>
                </tr>
                <tr>
                    <th scope="row">Benutzergruppen festlegen</th>
                    <td><select name="gruppen[]" multiple size="4">
                            <option value="Aktives Mitglied" selected>Aktive Mitglieder</option>
                            <option value="Passives Mitglied" selected>Passive Mitglieder</option>
                            <option value="Ehrenmitglied" selected>Ehrenmitglieder</option>
                            <option value="Sonstiges Mitglied" selected>Sonstige Mitglieder</option>
                        </select><br />
                    	<span class="description">Für welche Gruppen soll der Termin besimmt sein? (Wichtig, wenn die Mitgliederteilnahme aktiviert ist.)</span></td>
                </tr>
            </table>
            <?php
            wp_nonce_field( 'jbs-new-event' );
            submit_button('Termin hinzufügen', 'primary', 'newevent'); ?>
        </form>
	</div>
	<?php
}

function jbs_events_settings_knobelturnier_content() {
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}
  ?>
  <div class="wrap">
    <h2><?php esc_html_e('Einstellungen'); ?></h2>

    <h2 class="nav-tab-wrapper">
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
    </h2>

    <form method="post" action="options.php">
    <?php settings_fields('jbs-option-knobelturnier-group'); ?>
    <?php do_settings_sections('jbs-option-knobelturnier-group'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Anmeldungen freischalten</th>
                <td>
                	<input type="checkbox" name="knobelturnier-anmeldungen-aktiviert" <?php echo ((get_option('knobelturnier-anmeldungen-aktiviert') == "on") ? "checked" : "") ?> /><br />
                    <span class="description"><?php _e("Anmeldungen für das interne Knoblelturnier aktivieren?"); ?></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Datum</th>
                <td>
                    <input type="date" size="40" data-date="" data-date-format="DD MMMM YYYY" name="knobelturnier-datum" value="<?php echo esc_attr(get_option('knobelturnier-datum')); ?>" /><br />
                    <span class="description"><?php _e('<b>{{DATUM}}</b> kann im Editor verwendet werden.','jbs-events'); ?></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Ausrichter</th>
                <td>
                    <input type="text" size="40" name="knobelturnier-ausrichter" value="<?php echo esc_attr(get_option('knobelturnier-ausrichter')); ?>" /><br />
                    <span class="description"><?php _e('<b>{{AUSRICHTER}}</b> kann im Editor verwendet werden.','jbs-events'); ?></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Informationstext</th>
            	<td>
				    <?php
				    $content = get_option('knobelturniermessagetext') ;
				    $editor_id = "knobelturniermessagetext";
					wp_editor( html_entity_decode($content), $editor_id );
					?>
				    <span class="description"><?php _e('Wird angezeigt, wenn Anmeldungen deaktiviert sind.', 'jbs-events'); ?></span>
            	</td>
            </tr>
            <tr valign="top">
                <th scope="row">Formularfelder</th>
                <td>
                    <input type="checkbox" name="knobelturnier-feld-teamname"  <?php echo ((esc_attr(get_option('knobelturnier-feld-teamname'))) == 'on') ? 'checked' : ""; ?> /> Teamname<br />
                    <span class="description"><?php _e('<b>{{TEAM}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-1"  <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-1'))) == 'on') ? 'checked' : ""; ?> /> Spieler 1<br />
                    <span class="description"><?php _e('<b>{{SPIELER1}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-2" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-2'))) == 'on') ? 'checked' : ""; ?> /> Spieler 2<br />
                    <span class="description"><?php _e('<b>{{SPIELER2}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-3" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-3'))) == 'on') ? 'checked' : ""; ?> /> Spieler 3<br />
                    <span class="description"><?php _e('<b>{{SPIELER3}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-4" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-4'))) == 'on') ? 'checked' : ""; ?> /> Spieler 4<br />
                    <span class="description"><?php _e('<b>{{SPIELER4}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-ersatz" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-ersatz'))) == 'on') ? 'checked' : ""; ?> /> Ersatzspieler<br />
                    <span class="description"><?php _e('<b>{{SPIELERERSATZ}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-email" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-email'))) == 'on') ? 'checked' : ""; ?> /> E-Mail vom Ansprechpartner<br />
                    <span class="description"><?php _e('<b>{{ANSPRECHEMAIL}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br /><br />
                    <input type="checkbox" name="knobelturnier-feld-sp-ansprechpartner" <?php echo ((esc_attr(get_option('knobelturnier-feld-sp-ansprechpartner'))) == 'on') ? 'checked' : ""; ?> /> Name vom Ansprechpartner<br />
                    <span class="description"><?php _e('<b>{{ANSPRECHPARTNER}}</b> kann im Editor der Anmelde-Emails verwendet werden.','jbs-events'); ?></span><br />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Anmeldungen schicken an</th>
                <td>
                    <input type="email" size="40" name="knobelturnier-mail-anmeldungen" value="<?php echo esc_attr(get_option('knobelturnier-mail-anmeldungen')); ?>" /><br />
                    <span class="description"><?php _e("An diese E-Mail Adresse werden neue Anmeldungen geschickt"); echo "<br />"; _e("Um Das Formular zu benutzen, füge <b>[jbs_knobelturnier]</b> auf einer beliebigen Seite ein");?></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Bestätigungsmail senden</th>
                <td>
                    <input type="checkbox" size="40" name="knobelturnier-mail-anmeldungen-bestaetigung" <?php echo ((esc_attr(get_option('knobelturnier-mail-anmeldungen-bestaetigung'))) == 'on') ? 'checked' : ""; ?> />
                    <span class="description"><?php _e("Wenn aktiviert, wird eine Bestätigungsmail verschickt. <b>Wichtig: </b>Dazu muss das E-Mail Feld aktiviert sein.");?></span>
                </td>
            </tr>
            <!--<tr valign="top">
                <th scope="row">Anmeldung an E-Mail anhängen</th>
                <td>
                    <input type="checkbox" size="40" name="knobelturnier-mail-anmeldungen-anhang" <?php echo ((esc_attr(get_option('knobelturnier-mail-anmeldungen-anhang'))) == 'on') ? 'checked' : ""; ?> />
                    <span class="description"><?php _e("Die Anmeldungen werden als .txt Datei an die E-Mail angehangen und können so direkt in die Turniersoftware von Jakob eingelesen werden. Dies spart zusätzliche Zeit.");?></span>
                </td>
            </tr>-->
            <tr valign="top">
            	<th scope="row">Inhalt Anmeldungsmail</th>
            	<td>
				    <?php
				    $content = get_option('knobelturniermessageanmeldung') ;
				    $editor_id = "knobelturniermessageanmeldung";
					wp_editor( html_entity_decode($content), $editor_id );
					?>
				    <span class="description"><?php _e('Inhalt der E-Mail an die Ausrichter', 'jbs-events'); ?></span>
            	</td>
            </tr>
            <tr valign="top">
            	<th scope="row">Inhalt Bestätigungsmail</th>
            	<td>
				    <?php 
				    $content = get_option('knobelturniermessagebestaetigung') ;
				    wp_editor( html_entity_decode($content), 'knobelturniermessagebestaetigung');
				    ?>
				    <span class="description"><?php _e('Inhalt der Bestätigungsmail', 'jbs-events'); ?></span>
            	</td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>

  </div>
  <?php
}

function jbs_events_settings_css_content() {
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}
  ?>
  <div class="wrap">
    <h2><?php esc_html_e('Einstellungen'); ?></h2>

    <h2 class="nav-tab-wrapper">
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	  <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
    </h2>

    <form method="post" action="options.php">
    <?php settings_fields('jbs-option-css-group'); ?>
    <?php do_settings_sections('jbs-option-css-group'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Benutzerdefiniertes CSS:','jbs-events'); ?></th>
                <td>
                	<textarea name="jbs-events-css" style="width:100%;height:350px;"value="<?php echo esc_attr(get_option('jbs-events-css')); ?>"><?php echo esc_attr(get_option('jbs-events-css')); ?></textarea><br />
                	<span class="description">Kleine Änderungen am Design können hier vorgenommen werden.</span>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>

  </div>
  <?php
}

function jbs_events_settings_shortcodes_content(){
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
	{
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');
	}
  ?>
  <div class="wrap">
    <h2><?php esc_html_e('Einstellungen'); ?></h2>

    <h2 class="nav-tab-wrapper">
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	  <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
    </h2>
    <h3>Folgende Shortcodes stehen in diesem Plugin zur Verfügung. Sie können auf Seiten oder in Posts eingebunden werden.</h3>
    <form method="post" action="options.php">
    <?php settings_fields('jbs-option-shortcodes-group'); ?>
    <?php do_settings_sections('jbs-option-shortcodes-group'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Termine','jbs-events'); ?></th>
                <td>
                	<span class="description">Zum Einbinden einer Termintabelle kann <b>[jbs_events]</b> verwendet werden.<br />
                	Optional kann ein Jahr angegeben werden, z.B. <b>[jbs_events y=<?php echo date('Y'); ?>]</b></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Profil bearbeiten','jbs-events'); ?></th>
                <td>
                	<span class="description">Ein Formular zum Anzeigen/Ändern des Benutzerprofils kann <b>[jbs_profile]</b> verwendet werden.</span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Passwort ändern','jbs-events'); ?></th>
                <td>
                	<span class="description">Ein Formular zum Ändern des Benutzerpasswortes kann <b>[jbs_change_password]</b> verwendet werden.</span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Adressliste','jbs-events'); ?></th>
                <td>
                	<span class="description">Zum Einbinden einer Adressliste aller Benutzer kann <b>[jbs_addresses]</b> verwendet werden.</span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Loginformular','jbs-events'); ?></th>
                <td>
                	<span class="description">Zum Einbinden eines Loginformulars kann <b>[jbs_login]</b> verwendet werden.</span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Registrierungsformular','jbs-events'); ?></th>
                <td>
                	<span class="description">Zum Einbinden eines Registrierungsformulars kann <b>[jbs_register]</b> verwendet werden.</span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php esc_html_e('Anmeldeformular Knobelturnier','jbs-events'); ?></th>
                <td>
                	<span class="description">Zum Einbinden eines Anmeldeformulars kann <b>[jbs_knobelturnier]</b> verwendet werden.</span>
                </td>
            </tr>
        </table>
    </form>

  </div>
  <?php
}

function jbs_events_settings_log_content() {
	if( !current_user_can('edit_pages') )
	{
		wp_die( __('Du hast nicht die benötigten Rechte, um auf diese Seite zugreifen zu können!','jbs-events'));
	}

	if( isset( $_POST['deletelog']) && wp_verify_nonce($_REQUEST['_wpnonce'], 'jbs-delete-log' ) && $_POST['jbs-log'] != "")
	{
       global $wpdb;
       $table = $wpdb->prefix . 'jbs_events_log';
       $sql = "DROP TABLE $table;";
       $e = $wpdb->query($sql);
	   $charset_collate = $wpdb->get_charset_collate();
	   $sql3 = "CREATE TABLE $table (
	            id mediumint(9) NOT NULL AUTO_INCREMENT,
	            time datetime NOT NULL,
	            action text NOT NULL,
	            info text NOT NULL,
	            userid mediumint(9),
	            user text,
	            ip text NOT NULL,
	            UNIQUE KEY id (id)) $charset_collate;";

	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    if(count(dbDelta($sql3)) > 0 )
		Helper::settingsUpdated('Deine Einstellungen wurden gespeichert.');}
	
  ?>
  <div class="wrap">
    <h2><?php esc_html_e('Einstellungen'); ?></h2>

    <h2 class="nav-tab-wrapper">
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings">Registrierung</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-css">Style</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-knobelturnier">Knobelturnier</a>
	  <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-intern">Intern</a>
      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-shortcodes">Shortcodes</a>
	      <a class="nav-tab" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-smtp">E-Mail</a>
      <a class="nav-tab nav-tab-active" href="<?php echo admin_url() ?>admin.php?page=jbs-events-settings-log">Log</a>
    </h2>

    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
    <?php settings_fields('jbs-option-log-group'); ?>
    <?php do_settings_sections('jbs-option-log-group'); ?>
	<textarea style="margin-top:20px;width:100%;height:350px;" name="jbs-log" readonly><?php $this->getLog(); ?>
	</textarea>
        <?php wp_nonce_field('jbs-delete-log');
        //submit_button(esc_html__('Log leeren','jbs-events'), 'primary', 'deletelog');
        ?>
    </form>

  </div>
  <?php
}

public function getLog() {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_log';
    $results = $wpdb->get_results("SELECT * FROM $table ORDER BY id desc");
    $array = "";
    foreach ($results as $log) {

        $m = strtoupper($log->action);
        $tmp = "[$log->time][$m]\t$log->info\t(durch user: $log->user[$log->userid]) - IP:$log->ip \n\n";
        echo $tmp;
    }
}

	/**
	 * Screen options
	 */
	public function screen_option() {
		$option = 'per_page';
		$args   = [
			'label'   => 'Events',
			'default' => 5,
			'option'  => 'events_per_page'
		];
		add_screen_option( $option, $args );
		$this->events_obj = new Participant_Table();
	}
	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}

 class Helper{

		public static function settingsUpdated($message){
			$output = '<div class="updated"><p>'
			. esc_html($message) . '</p></div>';

			echo $output;
		}

		public static function updateFailed($message){
			$output = '<div class="error"><p>'
			. esc_html($message) . '</p></div>';

			echo $output;
		}

		public static function newLog($action, $time, $info, $id, $name, $ip) {
		    global $wpdb;
		    $table = $wpdb->prefix . 'jbs_events_log';
		    $wpdb->insert($table, array(
		        'time' => $time,
		        'action' => $action,
		        'info' => $info,
		        'userid' => $id,
		        'user' => $name,
		        'ip' => $ip
		            ), array(
		        '%s', '%s', '%s', '%d', '%s', '%s'
		    ));
		}

	}
?>
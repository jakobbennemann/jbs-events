<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php' );
require_once( $_SERVER['DOCUMENT_ROOT']  . '/wp-admin/includes/template.php');
require_once( dirname(__FILE__) . '/class-jbs-show-event-list.php' );


function get_Eventdescription($event_id) {
    global $wpdb;
    $db_table = $wpdb->prefix . 'jbs_events';

    //Abfrage
    $abfrage = $wpdb->get_row("SELECT description FROM " . $db_table . " WHERE id =" . $event_id . "");
    return $abfrage->description;
}


if (isset($_GET['action']) ) {
    $eventid = $_GET['event'];
    if ($_GET['action'] == 'show' && wp_verify_nonce($_REQUEST['nonce'], 'jbs_show_event')) {
        add_thickbox();
        ?>
        <div class = "wrap">
            <h2>Teilnehmer für "<?php echo get_Eventdescription($eventid); ?>"</h2>

            <table class="form-table">
                <tr>
                    <th><label for="schlagwerker_zusage">Zusagen Schlagwerker:</label><br />
                        <label for="schlagwerker_unsicher">Schlagwerker unsicher:</label><br />
                        <label for="schlagwerker_absage">Absagen Schlagwerker:</label></th>
                    <td><label for="schlagwerker_zusage_wert"><?php echo getZusagenSchlagwerker($eventid) ?></label><br />
                        <label for="schlagwerker_unsicher_wert"><?php echo getUnsicherSchlagwerker($eventid) ?></label><br />
                        <label for="schlagwerker_absage_wert"><?php echo getAbsageSchlagwerker($eventid) ?></label></td>
                    <th><label for="floetisten_zusage">Zusagen Flötisten:</label><br />
                        <label for="floetisten_unsicher">Flötisten unsicher:</label><br />
                        <label for="floetisten_absage">Absagen Flötisten:</label></th>
                    <td><label for="floetisten_zusage_wert"><?php echo getZusageFloetist($eventid) ?></label><br />
                        <label for="floetisten_unsicher_wert"><?php echo getUnsicherFloetist($eventid) ?></label><br />
                        <label for="floetisten_absage_wert"><?php echo getAbsageFloetist($eventid) ?></label></td>
                </tr>
            </table>

            <?php
            // Liste anlegen
            $teilnehmer = new JBS_Event_List();
            $teilnehmer->setEventID($eventid);

            //hier werden die Daten aus der DB geladen...
            //danach an die Liste übergeben
            //Tabelle ausgeben
            ?>
            <?php
            if (!isset($_POST['s'])) {
                $teilnehmer->setTeilnehmerProSeite(get_option('events_per_page'));
                
                $teilnehmer->prepare_items();
                $teilnehmer->display();
            } 
            $pdf_nonce = wp_create_nonce( 'jbs_pdf_event' );
            ?>
            <form method="post" action="<?php echo admin_url('admin.php?page=jbs-events&action=pdf&id=' . $eventid . '&nonce='.$pdf_nonce.'', 'https'); ?>">
            <?php 
                submit_button('als .pdf herunterladen', 'primary', 'print_pdf', false); ?> 
            </form>

            <?php echo '</div>'; ?>

        </div> <?php
    } else if ($_GET['action'] == 'edit' && wp_verify_nonce($_REQUEST['nonce'], 'jbs_edit_event')) {

        //Eventinfos laden
        global $wpdb;
        $table = $wpdb->prefix . 'jbs_events';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $result = $wpdb->get_row("SELECT * FROM $table WHERE id=$eventid", ARRAY_N);
        $desc = $result[2];
        $date = $result[1];
        $time_start = $result[3];
        $time_end = $result[4];
        $parti = $result[5];
        $info = $result[6];
        $info_part = $result[7];
        $link = $result[8];
        $gr_aktiv = $result[10];
        $gr_passiv = $result[11];
        $gr_ehren = $result[12];
        $gr_sonstige = $result[13];
        ?>
        <div class = "wrap">
            <h2>Termin "<?php echo $desc; ?>" bearbeiten</h2>
            <form method="post" action="<?php echo admin_url('admin.php?page=jbs-events', 'https'); ?>">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Bezeichnung: <span style='color:#d54e21;'>*</span></th>
                        <td><input type="text" name="event_description" value="<?php echo $desc; ?>" required /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Datum: <span style='color:#d54e21;'>*</span></th>
                        <td><input type="date" name="event_date" value="<?php echo $date; ?>" required /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Uhrzeit (Beginn):</th>
                        <td><input type="time" name="event_time_start" value="<?php echo $time_start; ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Uhrzeit (Ende):</th>
                        <td><input type="time" name="event_time_end" value="<?php echo $time_end; ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Teilnahmemöglichkeit: <span style='color:#d54e21;'>*</span></th>
                        <td><select name="event_participation" required>
                                <option value="1" <?php
                                if ($parti == 1) {
                                    echo 'selected';
                                }
                                ?>>mit Teilnahmemöglichkeit</option>
                                <option value="0"<?php
                                if ($parti == 0) {
                                    echo 'selected';
                                }
                                ?>>als Information</option>
                            </select></td>
                    </tr>
                    <tr>
                        <th scope="row">Eventinformation:</th>
                        <td><textarea rows="6" cols="40" name="event_info"><?php echo $info; ?></textarea></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Beitrag verlinken:</th>
                        <td><select name="event_link">

                                <option value="0">-- keine Verlinkung --</option>
                                <?php
                                global $post;
                                $args = array('order' => 'ASC', 'orderby' => 'title');
                                $lastposts = get_posts($args);
                                foreach ($lastposts as $post) :
                                    setup_postdata($post);
                                    $sel = "";
                                    if (get_permalink($post->ID) == $link) {
                                        $sel = "selected";
                                    }
                                    ?>
                                    <option value="<?php echo get_permalink($post->ID); ?>" 
                                            <?php echo $sel; ?>><?php the_title(); ?></option>

                                    <?php
                                endforeach;
                                wp_reset_postdata();
                                ?>

                            </select></td>
                    </tr>
                    <tr>
                        <th scope="row">Eingabefeld für Teilnehmer anzeigen:</th>
                        <td><input type="checkbox" name="event_participant_info" <?php
                            if ($info_part == 1) {
                                echo 'checked';
                            }
                            ?>></td>
                    </tr>
                    <tr>
                        <th scope="row">Benutzergruppen festlegen</th>
                        <td><select name="gruppen[]" multiple size="4">
                                <option value="Aktives Mitglied" <?php echo (($gr_aktiv) == 1) ? 'selected' : ""; ?>>Aktive Mitglieder</option>
                                <option value="Passives Mitglied"<?php echo (($gr_passiv) == 1) ? 'selected' : ""; ?>>Passive Mitglieder</option>
                                <option value="Ehrenmitglied"<?php echo (($gr_ehren) == 1) ? 'selected' : ""; ?>>Ehrenmitglieder</option>
                                <option value="Sonstiges Mitglied"<?php echo (($gr_sonstige) == 1) ? 'selected' : ""; ?>>Sonstige Mitglieder</option>
                            </select></td>
                    </tr>
                    <tr style="display:none;">
                        <th scope="row" ></th>
                        <td><input type="text" name="event_id" value="<?php echo $eventid; ?>" /></td>
                    </tr>
                </table>
                <?php wp_nonce_field( 'jbs-update-event' ); submit_button('Speichern', 'primary', 'updateevent'); ?>
            </form>
        </div>
    <?php
    } else if ($_GET['action'] == 'notify') {

        //Eventinfos laden
        global $wpdb;
        $table = $wpdb->prefix . 'jbs_events';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $result = $wpdb->get_row("SELECT * FROM $table WHERE id=$eventid", ARRAY_N);
        $gr_aktiv = $result[10];
        $gr_passiv = $result[11];
        $gr_ehren = $result[12];
        $gr_sonstige = $result[13];
        ?>

        <div class = "wrap">
            <h2>Erinnerungsmail für "<?php echo get_Eventdescription($eventid); ?>" verschicken</h2>

            <p>Wähle unten aus, an welche Benutzergruppen eine Erinnerungsmail verschickt werden soll.<br />
                Es werden nur die Gruppen angezeigt, für die du den Termin freigegeben hast.</p>
            <form method="post" action="<?php echo admin_url('admin.php?page=jbs-events', 'https'); ?>">

                <table class="form-table">
                    <tr>
                        <td>
                            <label for="gruppen">Gruppe(n) auswählen:</label></td><td>
                            <select name="gruppen[]" multiple><?php
                                $output = "";
                                if ($gr_aktiv == 1) {
                                    $output .= '<option value="Aktives Mitglied">Aktive Mitglieder</option>';
                                }
                                if ($gr_passiv == 1) {
                                    $output .= '<option value="Passives Mitglied">Passive Mitglieder</option>';
                                }
                                if ($gr_ehren == 1) {
                                    $output .= '<option value="Ehrenmitglied">Ehrenmitglieder</option>';
                                }
                                if ($gr_sonstige == 1) {
                                    $output .= '<option value="Sonstiges Mitglied">Sonstige Mitglieder</option>';
                                }
                                echo $output;
                                ?></select><br />
                            <span class="description"><?php _e("Wenn keine Gruppe(n) ausgewählt wurde(n), wird eine Mail an alle Gruppen verschickt."); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="status">Status:</label></td><td>
                            <input type="checkbox" name="status[]" value="1" checked /> Unsichere Teilnehmer
                            <input style="margin-left:5%;" type="checkbox" name="status[]" value="3" checked /> Mitglieder ohne Angabe<br />
                            <span class="description"><?php _e("Welche Mitglieder sollen eine Mail erhalten?"); ?></span></td>

                    </tr>
                    <tr style="display:none;">
                        <td>
                            <select name="groups[]" multiple><?php
                                $output = "";
                                if ($gr_aktiv == 1) {
                                    $output .= '<option value="Aktives Mitglied" selected>Aktive Mitglieder</option>';
                                }
                                if ($gr_passiv == 1) {
                                    $output .= '<option value="Passives Mitglied" selected>Passive Mitglieder</option>';
                                }
                                if ($gr_ehren == 1) {
                                    $output .= '<option value="Ehrenmitglied" selected>Ehrenmitglieder</option>';
                                }
                                if ($gr_sonstige == 1) {
                                    $output .= '<option value="Sonstiges Mitglied" selected>Sonstige Mitglieder</option>';
                                }
                                echo $output;
                                ?></select>
                            </td>
                    </tr>
                    <tr style="display:none;">
                        <th scope="row" ></th>
                        <td><input type="text" name="event_id" value="<?php echo $eventid; ?>" /></td>
                    </tr>
                </table>
        <?php submit_button('Erinnerungsmails verschicken', 'primary', 'notificationmail'); ?>
            </form>

        </div> 

        <?php
    }
}

function getZusagenSchlagwerker($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Schlagwerk'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=0");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlSchlagwerker = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlSchlagwerker++;
                    }
                }
            }
            return $anzahlSchlagwerker;
        }
    }
}

function getUnsicherSchlagwerker($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Schlagwerk'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=1");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlSchlagwerker = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlSchlagwerker++;
                    }
                }
            }
            return $anzahlSchlagwerker;
        }
    }
}

function getAbsageSchlagwerker($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Schlagwerk'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=2");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlSchlagwerker = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlSchlagwerker++;
                    }
                }
            }
            return $anzahlSchlagwerker;
        }
    }
}

function getZusageFloetist($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Flöte'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=0");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlFloetisten = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlFloetisten++;
                    }
                }
            }
            return $anzahlFloetisten;
        }
    }
}

function getUnsicherFloetist($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Flöte'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=1");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlFloetisten = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlFloetisten++;
                    }
                }
            }
            return $anzahlFloetisten;
        }
    }
}

function getAbsageFloetist($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_participation';
    $table_meta = $wpdb->prefix . 'usermeta';

    $results = $wpdb->get_results("SELECT user_id FROM $table_meta WHERE meta_value='Flöte'");
    $ids = array();
    foreach ($results as $user) {
        array_push($ids, $user->user_id);
    }
    $laenge = count($ids);
    if ($laenge == 0) {
        return 0;
    } else {
        $results = $wpdb->get_results("SELECT userid FROM $table WHERE eventid=$id AND status=2");
        $userids = array();
        foreach ($results as $value) {
            array_push($userids, $value->userid);
        }
        $count = count($userids);
        if ($count == 0) {
            return 0;
        } else {
            $anzahlFloetisten = 0;
            foreach ($userids as $userid) {
                foreach ($ids as $id) {
                    if ($userid == $id) {
                        $anzahlFloetisten++;
                    }
                }
            }
            return $anzahlFloetisten;
        }
    }
}

?>


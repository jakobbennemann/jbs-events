<?php

class JBS_Event_List extends JBS_List {

    var $found_data = array();
    var $anzahl_pro_seite = 0;
    var $eventid = '';
    var $data = array();


    /** Class constructor */
    public function __construct() {

        parent::__construct( [
            'singular' => __( 'Teilnehmer', 'jbs-events' ), //singular name of the listed records
            'plural'   => __( 'Teilnehmer', 'jbs-events' ), //plural name of the listed records
            'ajax'     => false //should this table support ajax?

        ] );

    }

    public function search_box( $text, $input_id ) { ?>
        <p class="search-box">
            <label class="screen-reader-text" for="<?php echo $input_id ?>"><?php echo $text; ?>:</label>
            <input type="search" id="<?php echo $input_id ?>" name="s" value="<?php _admin_search_query(); ?>" />
        <?php submit_button( $text, 'button', false, false, array('id' => 'search-submit') ); ?>
        </p>
    <?php }

    function get_columns() {
        $columns = array(
            'nr' => '#',
            'name' => 'Name',
            'instrument' => 'Instrument',
            'status' => 'Status',
            'note' => 'Notiz'
        );
        return $columns;
    }

    function setTeilnehmerProSeite($anzahl) {
        $this->anzahl_pro_seite = $anzahl;
    }

    function setEventID($id) {
        $this->eventid = $id;
    }


    /** Text displayed when no customer data is available */
    public function no_items() {
      _e( 'Keine Teilnehmer verfügbar.', 'jbs-events' );
    }

    /**
     * Returns the count of records in the database.
     *
     * @return null|string
     */
    public function record_count() {
        global $wpdb;
        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}jbs_events_participation WHERE eventid ={$this->eventid}";
        return $wpdb->get_var( $sql );
    }

    /** get all Events */
    public function getEvents( $per_page = 5, $page_number = 1 ){

        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}jbs_events_participation WHERE eventid ={$this->eventid}";

        if ( ! empty( $_REQUEST['orderby'] ) ) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }

        $sql .= " LIMIT $per_page";

        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


        $result = $wpdb->get_results( $sql, 'ARRAY_A' );

        return $result;

    }


    public function get_sortable_columns() {
        $sortable_columns = array(
            'nr' => array('nr', true),
            'name' => array('name', true),
            'instrument' => array('instrument', true),
            'status' => array('status', true),
            'note' => array('notice', true),
        );
        return $sortable_columns;
    }


    function get_participants($e_id, $per_page = 5, $page_number = 1) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'jbs_events_participation';
        $table_user = $wpdb->prefix . 'users';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $teilnehmer = array();
        $sql = "SELECT * FROM $table_name JOIN $table_user ON $table_name.userid=$table_user.ID WHERE $table_name.eventid=$e_id";

        if ( ! empty( $_REQUEST['orderby'] ) ) {
                    $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
                    $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
                }

        $results = $wpdb->get_results($sql);
        $tmp_nr = 1;
        foreach ($results as $user) {
            $s = "";

            if ($user->status == 0) {
                $s = "nimmt teil";
            } else if ($user->status == 2) {
                $s = "nimmt nicht teil";
            } else if ($user->status == 1) {
                $s = "unsicher";
            }
            
            $instrument = $wpdb->get_row("SELECT meta_value FROM $table_usermeta WHERE user_id=$user->ID AND meta_key='instrument'");
            

            $tmp = array('nr' => $tmp_nr, 'name' => $user->display_name, 'instrument' => $instrument->meta_value,
                'status' => $s, 'note' => $user->info_participant);
            $tmp_nr++;
            array_push($teilnehmer, $tmp);
        }
        return $teilnehmer;
    }

    function prepare_items() {

      $this->_column_headers = $this->get_column_info();

      $per_page     = $this->get_items_per_page( 'events_per_page', 10 );
      $current_page = $this->get_pagenum();
      $total_items  = self::record_count();


      $this->items = self::get_participants( $this->eventid, 10 ,$current_page );

    }

    function column_default($item, $column_name) {
        switch ($column_name) {
            case 'nr':
                return $item[$column_name];
            case 'name':
                return $item[$column_name];
            case 'instrument':
                return $item[$column_name];
            case 'status':
                return $item[$column_name];
            case 'note':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

}

?>
<?php

function addDeleteLog($user_id) {
    global $current_user;
    $user_delete = get_userdata($user_id);
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    insertLog('User deleted', $time, 'user-id:' . $user_id . '|name:' . $user_delete->display_name . '|mail:' . $user_delete->user_email, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
}

function addCreateLog($user_id) {
    global $current_user;
    $user_delete = get_userdata($user_id);
    wp_get_current_user();
    $time = date("Y-m-d H:i:s");
    insertLog('User created', $time, 'user-id:' . $user_id . '|name:' . $user_delete->display_name . '|mail:' . $user_delete->user_email, $current_user->ID, $current_user->display_name, $_SERVER['REMOTE_ADDR']);
}

function insertLog($action, $time, $info, $id, $name, $ip) {
    global $wpdb;
    $table = $wpdb->prefix . 'jbs_events_log';
    $wpdb->insert($table, array(
        'time' => $time,
        'action' => $action,
        'info' => $info,
        'userid' => $id,
        'user' => $name,
        'ip' => $ip
            ), array(
        '%s', '%s', '%s', '%d', '%s', '%s'
    ));
}

add_action('delete_user', 'addDeleteLog');
add_action('user_register', 'addCreateLog');

?>
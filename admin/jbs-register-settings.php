<?php
	add_action('admin_init', 'jbs_register_settings');
	if( !function_exists('jbs_register_settings')){
		function jbs_register_settings() { // whitelist options
		    register_setting('jbs-option-css-group', 'jbs-events-css');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-mail-anmeldungen');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-ausrichter');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-datum');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-typ');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-anmeldungen-aktiviert');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-mail-anmeldungen-bestaetigung');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-mail-anmeldungen-anhang');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-teamname');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-1');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-2');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-3');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-4');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-ersatz');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-ansprechpartner');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturnier-feld-sp-email');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturniermessageanmeldung');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturniermessagebestaetigung');
		    register_setting('jbs-option-knobelturnier-group', 'knobelturniermessagetext');
		    register_setting('jbs-option-group', 'mail_from');
		    register_setting('jbs-option-group', 'mail_from_name');
		    register_setting('jbs-option-group', 'mailer');
		    register_setting('jbs-option-group', 'mail_set_return_path');
		    register_setting('jbs-option-group', 'smtp_host');
		    register_setting('jbs-option-group', 'smtp_ssl');
		    register_setting('jbs-option-group', 'smtp_port');
		    register_setting('jbs-option-group', 'smtp_user');
		    register_setting('jbs-option-group', 'smtp_pass');
		    register_setting('jbs-option-group', 'smtp_auth');
		    register_setting('jbs-option-intern-group', 'intern-users');
		    register_setting('jbs-option-intern-group', 'intern-guest');
		    register_setting('jbs-option-intern-group', 'page-guests-redirect');
		    register_setting('jbs-option-intern-group', 'page-users-redirect');
		    register_setting('jbs-option-intern-group', 'erinnerungsmessagetext');
		    register_setting('jbs-option-general-group', 'standard-gruppe');
		    register_setting('jbs-option-general-group', 'smtp-name');
		    register_setting('jbs-option-general-group', 'smtp-mail');
		    register_setting('jbs-option-general-group', 'standard-aktivierung');
		    register_setting('jbs-option-general-group', 'register-new-admin');
		    register_setting('jbs-option-general-group', 'register-new-user');
		    register_setting('jbs-option-general-group', 'register-username');
		    register_setting('jbs-option-general-group', 'register-email');
		    register_setting('jbs-option-general-group', 'register-password');
		    register_setting('jbs-option-general-group', 'register-password2');
		    register_setting('jbs-option-general-group', 'register-street');
		    register_setting('jbs-option-general-group', 'register-nr');
		    register_setting('jbs-option-general-group', 'register-plz');
		    register_setting('jbs-option-general-group', 'register-ort');
		    register_setting('jbs-option-general-group', 'register-first-name');
		    register_setting('jbs-option-general-group', 'register-last-name');
		    register_setting('jbs-option-general-group', 'register-instrument');
		    register_setting('jbs-option-general-group', 'register-mobil');
		    register_setting('jbs-option-general-group', 'register-blacklist');
		}
	}

?>